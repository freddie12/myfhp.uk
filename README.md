# README #



### What is this repository for? ###

* website of the functional health practice, www.myfhp.uk



### How do I get set up? ###

php version 4.0 or greater is needed for this to run smoothly

1) config/config.inc.php.deleteme needs to be renamed to config/config.inc.php and change the localhost variables

2) mod rewrite needs to be enabled within the http_conf inside apache files. uncomment in php.ini

3) the database can be found inside database/fhp.sql. This needs to be imported into a mysql database

4) If errors still persist then please try to enable E_ALL inside the config/config.inc.php and debug neccessary



### Who do I talk to? ###

* joe.m.roberts@btinternet.com