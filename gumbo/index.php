<?php

    $par = "Anyway, like I was sayin', shrimp is the fruit of the sea. You can barbecue it, boil it, broil it, bake it, saute it. Dey's uh, shrimp-kabobs, shrimp creole, shrimp gumbo. Pan fried, deep fried, stir-fried. There's pineapple shrimp, lemon shrimp, coconut shrimp, pepper shrimp, shrimp soup, shrimp stew, shrimp salad, shrimp and potatoes, shrimp burger, shrimp sandwich. That- that's about it.";
    $arr = explode(' ', $par);

    if(isset($_POST['button'])) {
        $word = $_POST['input'];

        if(!empty($word)) {

            $cnt = 0;

            foreach($arr as $a) {

                $wordin = strtolower($a);

                if($a == 'shrimp') {
                    $arr[$cnt] = $word;
                }

                if(strtolower($a) == 'shrimp,') {
                    $arr[$cnt] = $word . ',';
                }

                if(strtolower($a) == 'shrimp-kabobs,') {
                    $arr[$cnt] = $word . '-kabobs,';
                }

                $output = '<div style="font-size:40px; padding:10px; width:90%; margin:10px; overflow:hidden; border:solid 1px #000;">';

                $cnt++;
            }
        } else {
            echo 'please enter a word!';
        }

        for($i=0;$i<sizeof($arr); $i++) {
            $output .= $arr[$i] . ' ';
        }

        $output .= '</div>';
    }

    $form = '
            <html>
            <head>
                <title>Gumbos</title>
            </head>
            <body>
                <form action="' . $_SERVER['PHP_SELF'] . '" method="post">
                    <label for="input">Favourite Food</label>
                    <input type="text" name="input" value=""/>
                    <input type="submit" name="button" value="submit"/>
                </form>
            </body>
            </html>
            ';

    echo $form . '</br>' . $output;
?>