<?php
    require_once('../config/config.php');
require_once(CONTROLLERS . 'Class._404.Controller.php');
require_once(MODELS . 'Class._404.Model.php');
require_once(VIEWS . 'Class._404.View.php');

$model = new _404Model('404 Not Found');
$view = new _404View($model);
$controller = new _404Controller($model, $view);


$controller->init();
$view->output();
?>