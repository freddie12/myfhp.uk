<?php
//for($i=0;$i<1;$i++){}
    require_once('../config/config.php');

    $user = trim(Functions::escapeInjection($_POST['user']));
    $recipe = trim(Functions::escapeInjection($_POST['recipe']));

    //format
    if(Functions::saveRecipe($user, $recipe)) {
        echo 1;
    } else echo 0;
?>