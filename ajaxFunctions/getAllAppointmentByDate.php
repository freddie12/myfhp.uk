<?php
require_once('../config/config.php');

global $db;
$returnStr = '';
$freeAppTime = array();
$date = str_replace('/', '-', $_POST['date']);
$lines = array();
array_push($lines, array('start' => $date . ' ' . START, 'end' => $date . ' ' . START));

$query = "SELECT
            start, end
          FROM
            appointment
          WHERE
            start like '" . $date . "%'
          ORDER BY
            start
          ASC
         ";


$res = $db->iterate($query);

if(!$res->isEmpty()) {
    foreach($db->iterate($query) as $line) {
        $start = $line->start;
        $end   = $line->end;
        //need to plus one minute onto every end time.
        $end   = strtotime($end) + 60;
        $end   = Functions::unixToTimestamp($end);
        //add onto array
        array_push($lines, array('start' => $start, 'end' => $end));
    }

    /**
     * sort into start and end times for available times.
     */



    for($i=0;$i<sizeof($lines);$i++) {
        $nextRow = $i+1;

        //if there is a next row continue
        if(!($nextRow == sizeof($lines))) {
            $previousEnd = $lines[$i]['end'];
            $nextStart   = $lines[$nextRow]['start'];

            if(canStartAtEnd($previousEnd, $nextStart)) {
                array_push($freeAppTime, array("start" => $lines[$i]['end'], "end" => $lines[$nextRow]['start']));
            }
        } else {
            $end = $date . ' ' . FINISH;
            if(canStartAtEnd($lines[$i]['end'], $end)) {
                array_push($freeAppTime, array("start" => $lines[$i]['end'], "end" => $date . ' ' . FINISH));
            }
        }
    }


    //start,end|start,end
    $returnStr = array();
    foreach($freeAppTime as $item => $key) {
        $from = date('H:i', strtotime($key['start']));
        $to   = date('H:i', strtotime($key['end']));
        array_push($returnStr, $from .  ' - ' . $to);
    }

    if(!isset($freeAppTime[0])) {
        $returnStr = -1;
    } else {
        $returnStr = implode('|', $returnStr);
    }
} else {
    $returnStr = 0;
}

echo $returnStr;


/**
 * @param $date
 * @param $firstAppTime
 * @return int
 *
 * If first app is at 07:15 then we cannot say that 07:00 - 07:15 is available as minimum app time is 30 mins
 * So will return false to signify that the first free appointment will have to be after the first free time.
 */
function canStartAtEnd($end, $start) {
    $end      = strtotime($end);
    $start    = strtotime($start);

    //1st app endMin <15> minus 2nd app startMin <30> = 15 -> isWithin30 therefore cannot start
    $diff = $start - $end;
    //convert seconds to minutes.
    $diff = $diff / 60;

    if($diff < 30) {
        return 0;
    } else {
        return 1;
    }

}
?>