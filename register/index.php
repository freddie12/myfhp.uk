<?php
require_once('../config/config.php');
require_once(CONTROLLERS . 'Class.Register.Controller.php');
require_once(MODELS . 'Class.Register.Model.php');
require_once(VIEWS . 'Class.Register.View.php');

$model = new RegisterModel('Register');
$view = new RegisterView($model);
$controller = new RegisterController($model, $view);


$controller->init();
$view->output();
