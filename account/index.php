<?php

//make sure you spell view correctly!
require_once('../config/config.php');
require_once(CONTROLLERS    . 'Class.Account.Controller.php');
require_once(MODELS         . 'Class.Account.Model.php');
require_once(VIEWS          . 'Class.Account.View.php');
require_once(APPOINTMENT    . 'Class.Appointment.php');



if(!LOGGED_IN) {
    $jsArray = array('jquery', 'main', 'jquery-ui.min', 'login');//set extra js which calls the login function
    $model   = new AccountModel('Account', 'main', $jsArray);
} else {
    $model   = new AccountModel('Account');
}

$view       = new AccountView($model);
$controller = new AccountController($model, $view);


(LOGGED_IN) ? $controller->init() : "";
$view->output();

?>