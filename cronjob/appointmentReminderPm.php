<?php
date_default_timezone_set("Europe/London");
require_once('../config/config.php');
//get tomorrows date
$tomorrow = date("Y-m-d", strtotime('+1 day'));


$log = 'Log: </br></br>';

/**
 * Create vars to use with the email
 */
//for($i=0;$i<1;$i++) {}
if($res      = Functions::getAppointmentByDate($tomorrow)) {
    foreach ($res as $row) {
        //if reminder has not already been sent
        if(!$row->reminder_sent && $row->confirmed) {
            //user details
            $user = new ClientUser($row->user);
            $emailUserName = $user->getFullName();
            $userEmail = $user->getEmail();

            //time
            $emailTimeFrom = date("H:i", strtotime($row->sDate));
            $emailTimeTo = date("H:i", (strtotime($row->eDate) + 60));
            $emailDate = Functions::dateAsStr($row->sDate);

            //get venue details
            $emailAddressBuildingName = $row->venue->getBuildingName();
            $emailAddressFirstLineOfAddress = $row->venue->getFirstLine();
            $emailAddressTown = $row->venue->getTown();
            $emailAddressCounty = $row->venue->getCounty();
            $emailAddressPostcode = $row->venue->getPostcode();

            //staff details
            $staff = Functions::getStaff($row->staffId);
            $emailStaffName = $staff->name;
            $emailStaffPhone = $staff->phone;
            $emailStaffEmail = $staff->email;
            $emailStaffEmailLink = 'mailto:' . $emailStaffEmail;

            require_once('../html/email.appointment.reminder.html');
            global $sendMail;
            $sendMail->carrierPigeon($userEmail, 'Appointment Reminder', $html, 'no-reply');

            $query = "UPDATE
                        appointment
                      SET
                        reminder_sent = '1'
                      WHERE
                        id = '" . $row->id . "'
                       ";
            // repeat if failed
            do{
                if($res = $db->query($query)) $log .= '<p>sent</p>';
                else $log .= '<p>not sent</p>';
            } while(!$res);
        }
    }

}

?>