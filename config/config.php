<?php
session_start();

/**
 * This config stays the same however there must be a config.inc
 * file which is unique to the developers local enviroment
 * and ignored by git.
 *
 * Therefore please set up the config.inc.php.deleteThisLine to your specific specifications.
 */
require_once('config.inc.php');
require_once(DBPATH.        'mysqldatabase.php');
require_once(DBPATH.        'mysqlresultset.php');
require_once(CONTROLLERS .  'Class.Controller.php');
require_once(MODELS .       'Class.Model.php');
require_once(VIEWS .        'Class.View.php');
require_once(APPOINTMENT .  'Class.Appointment.php');
require_once(CLASSES .      'users/Class.User.php');
require_once(CLASSES .      'users/Class.ClientUser.php');
require_once(CLASSES .      'functions/Class.Functions.php');
require_once(CLASSES .      'email/Class.SendMail.php');
require_once(CLASSES .      'recipe/Class.Recipe.php');
require_once(CLASSES .      'appointments/Class.Appointment.php');
require_once(CLASSES .      'venue/Class.Venue.php');
require_once(CLASSES .      'Mobile_Detect.php');

//detect device
$detect = new Mobile_Detect;

if($detect->isMobile()) {
    define('MOBILE', true);
    define('STYLESHEET', 'main');
    // set to mobile if you with to style differently for tablets
    // define('STYLESHEET', 'mobile');
} else {
    define('MOBILE', false);
    define('STYLESHEET', 'main');
}

// length of staff day. For the appointment scheduling
define('START', '09:30:00'); //day start
define('FINISH', '19:30:00');//day end

// Connect to db
$db = MySqlDatabase::getInstance();
$sendMail = new SendEmail();

$db->connect(DBHOST, DBUSER, DBPASS, DB);

/* Use this to update the password for the admin */

//$password = sha1('goodfood1');
//
//$query = "UPDATE client_user
//        SET password = '" . $password . "'
//        WHERE
//            email = 'admin'";
//$db->quickQuery($query);


/**
 * auto try to log user in here
 * with session.
 */
if(isset($_SESSION['session_user_id']) && !empty($_SESSION['session_user_id'])) {

    $userObj = new ClientUser($_SESSION['session_user_id']);

    if(Functions::unknownUser($userObj)) {
        //session variable may be set, but no account in db.
        //If name is null then remove session
        unset($userObj);
        define('LOGGED_IN', false);
    } else {
        //user account is in the db
        define('LOGGED_IN', true);
        //Admin will either be true or false.
        define('ADMIN', $userObj->getAdmin());
    }
}

/**
 * Log in the user with cookies if session is unavailable.
 */
else if(isset($_COOKIE['session_user_id']) && !empty($_COOKIE['session_user_id'])) {

    $_SESSION['session_user_id'] = $_COOKIE['session_user_id'];

    $userObj = new ClientUser($_SESSION['session_user_id']);

    if(Functions::unknownUser($userObj)) {
        //session variable may be set,
        //but no account in db.
        //If name is null then remove session
        unset($userObj);
        define('LOGGED_IN', false);
    } else {
        //user account in db
        define('LOGGED_IN', true);
        //Admin will either be true or false.
        define('ADMIN', $userObj->getAdmin());
    }
//not a user
} else {
    define('LOGGED_IN', false);
    define('ADMIN', '0');
}


?>