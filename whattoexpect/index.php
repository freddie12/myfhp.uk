<?php

require_once('../config/config.php');
require_once(CONTROLLERS . 'Class.Expect.Controller.php');
require_once(MODELS . 'Class.Expect.Model.php');
require_once(VIEWS . 'Class.Expect.View.php');

$model = new ExpectModel('What To Expect');
$view = new ExpectView($model);
$controller = new ExpectController($model, $view);
//echo $_SERVER['REQUEST_URI'];
$controller->init();
$view->output();

?>