
var doc = $(document);
var page = $('html');
var displayed = false;
var contact = $('#magic-contact');
var children = $('#magic-contact *:not(.different)');
var allChildren = $('#magic-contact *');
var loginChildren = $('#login-box *:not(#login-success)');
var adminPanel = $('#admin-panel');
var rightPos = 0;
var topPos = 200;
var pos = true;
var cnt = 0;
var adminDisplay = false;
var auth;
var gv;
var DAYSTART = '09:30';
var DAYEND   = '19:30';


doc.ready( function() {
    contact.css({height:0});
    getMagicContactDetails();
//    fly();
    $('#calendar').fullCalendar({
        events: 'https://www.google.com/calendar/feeds/alexcharlesroberts%40gmail.com/private-3cbdf5e63143339a915f957b558e3f9e/basic'
    })
    $( "#datepicker" ).datepicker();
    $('#blur-screen').css({height:doc.height()});
    $('#maidenhead').css("background", "#9D3841");
})

$(window).resize(function() {
    loginBox = '#login-box';
    loginBox = getElementDimension(loginBox);
    setElementPosition(loginBox, getBoxPosition('#login-box'));
})

function displayAddress(el, from) {
    //reset all others
    $('#address1, #address2, #address3, #address4').hide();
    $('#addresslist .diff').css('background-color', '');
    $('#addresslist h3').css('color', '#474747');
    $('#' + from).css({'background-color': '#9D3841'});
    $('#' + from + ' h3').css({ 'color': '#fff'});
    $('#' + el).show();
}

function pageFade() {
    page.fadeOut(500);
}

function toggleAdminPanel() {
    if(!adminDisplay) {
        blurScreen('blur');
        adminPanel.delay(20).animate({left:0}, 700, 'easeInOutCubic');
        adminDisplay = true;
    } else {
        adminPanel.animate({left:-345}, 700, 'easeInOutCirc');
        blurScreen('unblur');
        adminDisplay = false;
    }
}


function getWindowDimension() {
    dimension = new Array();
    dimension[0] = $(window).width();
    dimension[1] = $(window).height();
    return dimension;
}

function getElementDimension(element) {
    dimension = new Array();
    dimension[0] = $(element).outerWidth();
    dimension[1] = $(element).outerHeight();
    return dimension;
}

function getBoxPosition(el) {
    winDimension = getWindowDimension();
    boxDimension = getElementDimension(el);
    pos = new Array();
    pos[0] = (winDimension[0]/2)-boxDimension[0]/2;
    pos[1] = (winDimension[1]/2)-boxDimension[1]/2;
    return pos;
}

function setElementPosition(el, dimension) {
    $(el).css({left:dimension[0], top:dimension[1]});
}

function displayLogin() {
    body = $('#full-container').height();
    $('#blur-screen').css('height', body);
    pos = getBoxPosition('#login-box');
    blurScreen('blur');
    $('#login-box').delay(500).css({left:pos[0], top:pos[1]}).fadeIn(500);
    loginChildren.delay(500).fadeIn(800);
}

function removeLogin() {
    loginChildren.fadeOut(500);
    $('#login-box').fadeOut(600, function() {
        blurScreen('unblur');
    });

}

function blurScreen(direction) {
    switch(direction) {
        case 'blur':
            $('#blur-screen').fadeIn(400);
            break;
        case('unblur'):
            $('#blur-screen').fadeOut(400);
            break;
    }

}


function fly() {

    $('#fly').css({right:rightPos, top:topPos});

    rightPos++;

    if(pos) {
        topPos++;
        cnt++;
    }
    else {
        topPos--;
        cnt--;
    }

    if(cnt >= 100) pos = false;
    if(cnt <=   0) pos = true;


    if(rightPos<900) {
        setTimeout(function() {fly();}, 100);
    }

}

function getFormDetails() {
    var details = new Array();
    details['method']       = $('textarea#method').val();
    details['ingredients']  = $('textarea#ingredients').val();
    details['title']        = $('input#title').val();
    details['description']  = $('input#description').val();

    return details;
}

function addRecipe(type, id) {
    var edit = false;

    switch(type) {
        case 'edit':
            phpFile     = 'editRecipe.php';
            setVar(id);
            edit        = true;
            break;
        case 'add':
            phpFile     = 'addRecipe.php';
    }

    resetWarning('#title-warning');
    resetWarning('#ingredients-warning');
    resetWarning('#method-warning');
    details = getFormDetails();
    if(details['title'] == '') {
        setWarning('#title-warning', ' *please fill title');
        return;
    } else if(details['ingredients'] == '') {
        setWarning('#ingredients-warning', ' *please fill ingredients');
        return;
    } else if(details['method'] == '') {
        setWarning('#method-warning', ' *please fill method');
        return;
    }
    $('#loading-gif').html('<p class="left inline">Loading, please wait</p><img src="../image/icon/loader.gif"/>');
    data =
    {
            'i' : details['ingredients'],
            't' : details['title'],
            'd' : details['description'],
            'm' : details['method'],
            'id': id
    };
    $.ajax({
        type: "POST",
        url: "../ajaxFunctions/" + phpFile,
        data: data,
        dataType: "JSON",
        success: function(res) {
            if(!edit) setVar(res);
            if(res >= 1) {
                document.location = '?action=recipes&i=' + getVar();
            } else {
                alert('Sorry something went wrong! Please try again.');
            }
        }
    });
}

function editPhone(id) {

    var phone = $('#phone').val();
        data =
        {
            'id'    : id,
            'phone' : phone
        };
        //ajax
        $.ajax({
            type: 'POST',
            data: data,
            url : '../update/editPhone.php',
            datatype: "JSON",
            success: function(res) {
                if(res == 1) {
                    //updated
                    message = "Phone number updated.";
                    button  = '<div class="global-button clear left push_twentyfive" style="width:100px"><a class="white" href="/account"><h6>Back</h6></a></div>';
                    html = '<div class="left push_ten"><img src="../image/icon/tick-big.png" style="width:48px; height:37px;"/></div><div class="left left_twenty"><h1 class="tahoma red left">Thank you!</h1><p class="push_ten clear left">' + message + '</p>' + button + '</div>';
                    $('#ajaxRes').html(html);
                } else if(res==0) {
                    //db error
                    setWarning($('#warning'), '* Please enter a valid phone number');
                } else {
                    setWarning($('#warning'), '* Unable to update the database. Please try again');
                }
            }
        })
}

function editName(id) {

    fname = $('#forename').val();
    lname = $('#surname').val();

    if(fname != '') {
        if(lname != '') {
            if(/^[a-zA-Z]+$/.test(fname)) {
                if(/^[a-zA-Z]+$/.test(lname)) {
                    data =
                    {
                        'id'    : id,
                        'fname' : fname,
                        'lname' : lname
                    };
                    //ajax
                    $.ajax({
                        type: 'POST',
                        data: data,
                        url : '../update/editName.php',
                        datatype: "JSON",
                        success: function(res) {
                            if(res==1) {
                                //updated
                                message = "Name updated."
                                button  = '<div class="global-button clear left push_twentyfive" style="width:100px"><a class="white" href="/account"><h6>Back</h6></a></div>';
                                html = '<div class="left push_ten"><img src="../image/icon/tick-big.png" style="width:48px; height:37px;"/></div><div class="left left_twenty"><h1 class="tahoma red left">Thank you!</h1><p class="push_ten clear left">' + message + '</p>' + button + '</div>';
                                $('#ajaxRes').html(html);
                            } else if(res==0) {
                                //db error
                                setWarning($('#warning'), '* Please try again');
                            } else {
                                //doesn't like the string
                                setWarning($('#warning'), '* Please use a different name');
                            }
                        }
                    })
                } else {
                    //lname not letters
                    setWarning($('#lname-warning'), '* Cannot have numbers in name');
                }
            } else {
                //fname not letters
                setWarning($('#fname-warning'), '* Cannot have numbers in name');
            }
        } else {
            //empty lname
            setWarning($('#lname-warning'), '* Please fill');
        }

    } else {
        //empty fname
        setWarning($('#fname-warning'), '* Please fill');
    }
}

function editEmail(id) {
    setWarning($('#emailWarning'), '');
    setWarning($('#email-confirm-warning'), '');
    setWarning($('#warning'), '');
    newEmail             = $('#newEmail').val();
    newEmailConfirmation = $('#newEmailConfirmation').val();
    if(newEmail != '') {
        if(newEmailConfirmation != '') {
            if(newEmail == newEmailConfirmation) {
                data =
                {
                    'id': id,
                    'email': newEmail
                };
                $.ajax({
                    type: "POST",
                    data: data,
                    url: '../update/editEmail.php',
                    datatype: "JSON",
                    success: function (res) {

                        if (res == 1) {
                            //updated
                            message = "Please go to your new email address and follow the instructions."
                            button = '<div class="global-button clear left push_twentyfive" style="width:100px"><a class="white" href="/account"><h6>Back</h6></a></div>';
                            html = '<div class="innerBox back-white lightBoxShadow"><div class="left push_ten"><img src="../image/icon/tick-big.png" style="width:48px; height:37px;"/></div><div class="left left_twenty"><h1 class="tahoma red left">Almost done</h1><p class="push_ten clear left">' + message + '</p>' + button + '</div></div>';
                            $('#ajaxRes').html(html);
                        } else if (res == 0) {
                            //db error
                            setWarning($('#warning'), '* Please try again');
                        } else if(res ==4) {
                            setWarning($('#warning'), '* This email is already in use');
                        } else {
                            //doesn't like the string
                            setWarning($('#warning'), '* This is not a valid email address');
                        }
                    }
                })
            } else {
                setWarning($('#email-confirm-warning'), '* Email doesn\'t match');
            }
        } else {
            //lname not letters
            setWarning($('#email-confirm-warning'), '* Please fill this field');
        }
    } else {
        //fname not letters
        setWarning($('#emailWarning'), '* Please fill this field');
    }



}

function changeAppStatus(id, status) {
    $.ajax({
        type: 'POST',
        url: '../ajaxFunctions/updateAppointmentStatus.php',
        data: {"id":id,"status":status},
        dataType: 'JSON',
        success: function(res) {

            switch(status) {
                case '-1':
                    $('#s').html("<img src='../image/icon/scheduled.png' alt='error'>");
                    $('#a').html("<img src='../image/icon/tick-grey.png' alt='error'>");
                    $('#m').html("<img src='../image/icon/cross-grey.png' alt='error'>");
                    break;
                case '0':
                    $('#s').html("<img src='../image/icon/scheduled-grey.png' alt='error'>");
                    $('#a').html("<img src='../image/icon/tick-grey.png' alt='error'>");
                    $('#m').html("<img src='../image/icon/cross.png' alt='error'>");
                    break;
                case '1':
                    $('#s').html("<img src='../image/icon/scheduled-grey.png' alt='error'>");
                    $('#a').html("<img src='../image/icon/tick.png' alt='error'>");
                    $('#m').html("<img src='../image/icon/cross-grey.png' alt='error'>");
                    break;


            }
            displayMessage('Appointment Status Changed')

        }

    })
}

function getVar() {
    return gv;
}

function setVar(v) {
    gv = v;
}


function deleteRecipe(id) {
    if(! (id == '')) {
        if(confirm('Are you sure you want to delete this Recipe?')) {
            //delete
            $.ajax({
                type: 'POST',
                url: '../ajaxFunctions/deleteRecipe.php',
                data : {"id" : id},
                dataType : 'JSON',
                success : function() {
                    message = 'Your recipe is no more!'
                    button  = '<div class="global-button clear left push_twentyfive" style="width:100px"><a class="white" href="?action=recipes"><h6>Back</h6></a></div>';
                    html = '<div class="innerBox back-white lightBoxShadow"><div class="left push_ten"><img src="../image/icon/tick-big.png" style="width:48px; height:37px;"/></div><div class="left left_twenty"><h1 class="tahoma red left">Thank you!</h1><p class="push_ten clear left">' + message + '</p>' + button + '</div></div>';
                    $('#ajaxResult').html(html);
                }
            });

        }
    }
}





function validateFormFeild(defaultText, id, textarea) {
    var current = getMagicContactDetails();//return text inside form
    var element;

    if(!textarea) element = '#'+id;
    else element          = 'textarea#'+id;

    if(current[id]==defaultText) $(element).val('');
    else if(current[id]=='') $(element).val(defaultText);
    else {}
}



function getMagicContactDetails() {
    var details = new Array();
    details['email']        = $("input#email").val();
    details['subject']      = $("input#subject").val();
    details['body']         = $("textarea#body").val();
    details['search']       = $("#search").val();
    details['recipeSearch'] = $("#recipeSearch").val();

    return details;
}


function displayContact(speed, animation) {
    if(!displayed) {
        window.scrollTo(0, 0);
        contact.css({display: 'block'}).animate({height:534}, speed, animation);
        children.fadeIn(1100);
        displayed = true;
    } else {
        removeContact(1300, 'easeOutQuint');
    }
}

function removeContact(speed, animation) {
    if(displayed) {
        allChildren.fadeOut(1100);
        contact.delay(200).animate({height:0}, speed, animation);
        displayed = false;
    } else {
        displayContact();
    }
}



function getLoginDetails() {
    var details = new Array();
    details['email'] = $("#login-email").val();
    details['password'] = $("#login-password").val();
    details['checkbox'] = document.getElementById("checkbox").checked;
    return details;
}

function login() {
    var data = getLoginDetails();
    cont = true;

    emailWarning = $('#email-warning')
    passwordWarning = $('#password-warning');

    resetWarning(emailWarning);
    resetWarning(passwordWarning);

    if(data['email'] == '') {
        setWarning(emailWarning, '* please enter valid email');
        cont = false;
    }

    if(data['password'] == '') {
        setWarning(passwordWarning, '* please enter valid password');
        cont = false;
    }

    if(cont) {
        data = "email="+data['email']+"&password="+data['password']+"&checkbox="+data['checkbox'];
        $.ajax({
            type: "POST",
            url: "../authentication/login.php",
            data: data,
            success: function(response) {
                if(response != 0 && response != -1) {
                        //login
                        loginChildren.fadeOut(500, function() {
                            $('#login-success').html('<h1>Welcome ' + response + '</h1>');
                            $('#login-success').fadeIn(500, function() {
                                $('#login-success').delay(200).fadeOut(500, function() {
                                    setTimeout(removeLogin(), 500);
                                });
                                setTimeout(function(){window.location.href = '../account';}, 1000);
                            });
                        });
                } else if(response == -1) {
                    //unverified account
                    setWarning($('#email-warning'), 'Account un-verified. Open registration email and follow the instructions');
                } else {
                        //no account res = 0
                        setWarning($('#email-warning'), '* please enter valid email address and password');
                        $("#login-password").val('');
                }
            }
        });
    }
}

$('#datepicker').datepicker( {
    minDate: new Date(),
    beforeShowDay: $.datepicker.noWeekends,
    onSelect: function() {
        //call available dates
        date = $('#datepicker').val();
        showAvailableTimeSlots(date);
    }
})


function displayAppointmentForm() {
    $('#submit').css({display: 'none'});
    $('#available-times').html('<p>Please select a date to view available times</p>');
    $('#appointmentFormContainer').fadeIn(1000, 'easeInOutCubic');
}

function showAvailableTimeSlots(date) {
    //get all appointments on this day
    date = 'date=' + date;
    $('#available-times').html('<img src="../image/icon/loader.gif"/>');
    $.ajax({
        type:"POST",
        url: "../ajaxFunctions/getAllAppointmentByDate.php",
        data : date,
        success: function(str) {
            html = '<p>Times that are available on the selected date are displayed below:</p></br>';

            if(str == 0) {
                html += '<p>' + DAYSTART + ' - ' + DAYEND + '</p>';
            } else if(str == -1) {
                html = '<p>Sorry we are fully booked on this day</p>';
            } else {
                //split times into array
                times = str.split("|");
                html = '<p>Times that are available on the selected date are displayed below</p>';
                html += '</br>';
                for(i=0;i<times.length;i++) {
                    html += '<p>between: ';
                    html += times[i];
                    html += '</p></br>';
                }
            }
            $('#available-times').html(html);

        }
    })
}

function getAppointmentByDate(date) {
    document.location = '?action=appointments&date=' + date;
}


function addBreak(allDay) {
    var data = new Array();
    if(!allDay) {
        data['sDate'] = $("#datepicker").val() + ' ' + $("#hour").val() + ':' + $("#minute").val() + ':00';
        //minus 1 minute
        tillMin  = parseInt($("#tillmin").val())-1;
        tillHour =  parseInt($("#tillhour").val());
        if(tillMin<0) {
            tillMin = 59;
            tillHour-=1;
            if(tillHour < 10) tillHour = "0" + tillHour;
        }
        tillMin = (tillMin).toString();
        data['eDate'] = $('#datepicker').val() + ' ' + tillHour + ':' + tillMin + ':00';
    }
    else {
        data['sDate'] = $('#datepicker').val() + " 09:30:00";
        data['eDate'] = $('#datepicker').val() + " 19:30:00";
    }

    setWarning($('#date-warning'), '');
    data = "s="+data['sDate']+"&e="+data['eDate'];
    setAuth(data['auth']);
    $.ajax({
        type: "POST",
        url: "../update/addBreak.php",
        data: data,
        success: function(res) {
            if(res == 1) {
                message = "The break has been added into your schedule.";
                button  = '<div class="global-button clear left push_twentyfive" style="width:100px"><a class="white" href="?action=appointments&date=' + $('#datepicker').val() + '"><h6>Back</h6></a></div>';
                html = '<div class="background-white-box push_seventyfive"><div class="left push_ten"><img src="../image/icon/tick-big.png" style="width:48px; height:37px;"/></div><div class="left left_twenty"><h1 class="tahoma red left">Thank you!</h1><p class="push_ten clear left">' + message + '</p>' + button + '</div></div>';
                //added appointment
                $('#ajaxResult').html(html);
            } else if(res == 0) {
                //date clash
                setWarning($('#date-warning'), '* Time slot unavailable, please try again.');
            } else if(res == 2) {
                setWarning($('#date-warning'), '* Date is in the past, please try again.');
            }
        }
    });
}

function addNonUser() {
    setWarning($('#date-warning'), '');
    // set up the form to save fname lname telephone
    var fname = $('#fname').val();
    var sname = $('#sname').val();
    var phone = $('#phone').val();
    if($.isNumeric(phone)) {

        if (fname != '' && sname != '') {

            // fname, sname, phone, start, end, user, staff, venue, auth
            var data = "fname=" + fname + "&sname=" + sname + "&phone=" + phone;

            $.ajax({
                type: "POST",
                url: "../ajaxFunctions/saveNonUser.php",
                data: data,
                success: function (res) {
                    if (res > 0) {
                        goToPage("?action=singleuser&i="+res);
                    } else {
                        // failed
                    }
                }

            });
        } else {
            setWarning("#date-warning", "Please fill all fields" );
        }
    } else {
        setWarning("#date-warning", "Please use a valid phone number." );
    }
}


function addAppointment(auth) {

        var data = getAppointmentFormDetails();

        setWarning($('#date-warning'), '');
        data = "s="+data['sDate']+"&e="+data['eDate']+"&u="+data['user']+"&st="+data['staff']+"&v="+data['venue']+"&auth="+auth;
        setAuth(data['auth']);
        $.ajax({
            type: "POST",
            url: "../update/addAppointment.php",
            data: data,
            success: function(res) {
                if(res == 1) {
                    if(auth == 'admin') {
                        message = "The appointment has been added and confirmation has been sent to your client.";
                        button  = "";
                    } else {
                        message = "Your appointment has been arranged. Once we confirm your booking you will receive an email confirmation."
                        button  = '<div class="global-button clear left push_twentyfive" style="width:100px"><a class="white" href="?action=viewAllAppointments"><h6>Back</h6></a></div>';
                    }
                    html = '<div class="innerBox back-white lightBoxShadow"><div class="left push_ten"><img src="../image/icon/tick-big.png" style="width:48px; height:37px;"/></div><div class="left left_twenty"><h1 class="tahoma red left">Thank you!</h1><p class="push_ten clear left">' + message + '</p>' + button + '</div></div>';
                    //added appointment
                    $('#ajaxResult').html(html);
                } else if(res == 0) {
                    //date clash
                    setWarning($('#date-warning'), '* Time slot unavailable, please try again.');
                } else if(res == 2) {
                    setWarning($('#date-warning'), '* Date is in the past, please try again.');
                }
            }
        });
}



function setAuth(a) {
    auth = a;
}

function setWarning(el, warning) {
    $(el).text(warning);
}

function resetWarning(el) {
    $(el).empty();

}

function goToPage($url) {
    document.location = $url;
}









function getAppointmentFormDetails() {
    var details = new Array();
    //datepicker holds hidden form with the date
    sDate       = $("#datepicker").val() + ' ' + $("#hour").val() + ':' + $("#minute").val() + ':00';
    duration    = getEndTime($("#hour").val(), $("#minute").val(), $("#duration").val());
    eDate       = $('#datepicker').val() + " " + duration[0]+":"+duration[1]+":00";

    uId         = $('#userId').val();
    sId         = $('#staff').val();

    venue       = $('#venue').val();



    details['sDate']    = sDate;
    details['eDate']    = eDate;
    details['user']     = uId;
    details['staff']    = sId;
    details['venue']    = venue;

    return details;
}

function getEndTime(startHour, startMin, duration) {
    endArr        = ((((parseInt(startHour, 10)*60) + parseInt(startMin, 10) + parseInt(duration, 10)) / 60).toFixed(2)).toString().split(".");
    endHour       = endArr[0];
    endMin        = Math.round(Number(endArr[1] / 100 *60)).toString().substr(0, 2);


    if(endMin == 0) {
        endMin = 59;
        endHour--;
    } else {
        endMin--;
    }

    //minus 1 minute so another appointment can be added straight away

    if(endHour < 10) {
        endHour = '0' + endHour;
    }

    if(endMin < 10) {
        endMin = '0' + endMin;
    }

    time = new Array(endHour, endMin);

    return time;
}

function openDropDown(id){
    document.getElementById(id).style.display = "block";
}

function closeDropDown(id){
    document.getElementById(id).style.display = "none";
}

function sendContactDetails() {
    var data = getMagicContactDetails();

    var ok = true;

    emailWarning = $('#contact-email-warning')
    subjectWarning = $('#contact-subject-warning');
    bodyWarning = $('#contact-body-warning');
    resetWarning(bodyWarning);
    resetWarning(emailWarning);
    resetWarning(subjectWarning);

    if(data['email'] == '' || data['email'] == 'Your@email.com') {

        ok = false;
    } else if(data['subject'] == '' || data['subject'] == 'Subject') {

        ok = false;
    } else if(data['body'] == '' || data['body'] == 'Your message...') {

        ok = false;
    }

    if(ok) {
        data = "email="+data['email']+"&subject="+data['subject']+"&body="+data['body'];
        $.ajax({
            type: "POST",
            url: "../ajaxFunctions/sendMessage.php",
            data: data,
            success: function(res) {
                if (res == 1) {
                    resetContactForm();
                    removeContact(1300, 'easeOutQuint');
                    displayMessage('Message Sent!');
                } else {
                    displayMessage('Message not sent. Try again');
                }
            }

        });
    } else {
        displayMessage('Please fill all fields');
    }


}

function editBreak(data) {
    if(confirm("DELETE BREAK?")) {
        //DELETE APPOINTMENT
        data = "id=" + data;
        $.ajax({
            type: "POST",
            url: "../update/deleteBreak.php",
            data: data,
            success: function(res) {
                location.reload();
            }
        })
    }
}

function resetContactForm() {
    $("input#email").val('Your@email.com');
    $("input#subject").val('Subject');
    $("textarea#body").val('Your message...');
}

function displayMessage(message) {
    $('#message-pop').html('<h3 class="white">' + message + '</h3>');
    pos = getBoxPosition('#message-pop');
    $('#message-pop').delay(700).css({left:pos[0], top:pos[1]}).fadeIn(1000);
    removeMessage();
}

function saveRecipe(user, recipe) {
    jsLink = "'" + user + "', '" + recipe + "'";
    $.ajax({
        type: 'POST',
        url: '../ajaxFunctions/saveRecipe.php',
        data: {"user":user,"recipe":recipe},
        dataType: 'JSON',
        success: function(res) {
            $('#savedButton').html('<div class="decision-button back-red clear" onclick="removeRecipe(' + jsLink + ')"><h3 class="white align-center">Remove this from MyRecipes</h3></div>')
        }
    })
}

function removeRecipe(user, recipe) {
    jsLink = "'" + user + "', '" + recipe + "'";
    $.ajax({
        type: 'POST',
        url: '../ajaxFunctions/removeRecipe.php',
        data: {"user":user,"recipe":recipe},
        dataType: 'JSON',
        success: function(res) {
            $('#savedButton').html('<div class="decision-button back-dark-green clear" onclick="saveRecipe(' + jsLink + ')"><h3 class="white align-center">Save this to MyRecipes</h3></div>')
        }
    })
}

function removeMessage() {
    $('#message-pop').delay(700).fadeOut(1200);
}


function cancelAppointment(id, auth) {
    if (auth == 'user') {
        data = 'id=' + id;
        $.ajax({
            type: "POST",
            url: "../ajaxFunctions/appointmentDate.php",
            data: data,
            success: function (res) {
                var mSecondsIn24Hrs = 86400000;
                var now = (new Date).getTime();
                res = res * 1000;
                var difference = res - now;
                if (difference < mSecondsIn24Hrs) {
                    $('#cancelWarning').html('Sorry but you are not permitted to cancel within 24 hours of arrangement. Please contact us <a class="green" href="mailto:info@myfhp.com">HERE</a>')
                }
                else {
                    confirmCancelAppointment(id, auth);
                }
            }
        });
    } else {
        confirmCancelAppointment(id, auth);
    }
}

 function confirmCancelAppointment(id, auth) {

        if (confirm('Are you sure you wish to cancel this appointment?')) {
        //get id of user for href
        //  take query string and split it
            var items = location.search.substr(1).split("&");
        //      split each property = value
                tmp = items[1].split("=");
        //      take the value which is the id
                urlId = decodeURIComponent(tmp[1]);
            userId = decodeURIComponent(items[2].split("=")[1]);

            //remove from database
            data = 'id=' + id + '&auth=' + auth;
            setAuth(auth);
            $.ajax({
                type: "POST",
                url: "../update/cancelAppointment.php",
                data: data,
                success: function (res) {
                    if (res == 1) {
                        if (auth == 'admin') {
                            message = "The appointment has been cancelled and the client has been notified.";
                            button  = '<div class="global-button clear left push_twentyfive" style="width:100px"><a class="white" href="?action=singleuser&i=' + userId + '"><h6>Back</h6></a></div>';
                        } else {
                            message = "The appointment has been cancelled."
                            button  = '<div class="global-button clear left push_twentyfive" style="width:100px"><a class="white" href="?action=viewAllAppointments"><h6>Back</h6></a></div>';
                        }
                        html = '<div class="innerBox back-white lightBoxShadow"><div class="left push_ten"><img src="../image/icon/tick-big.png" style="width:48px; height:37px;"/></div><div class="left left_twenty"><h1 class="tahoma red left">Thank you!</h1><p class="push_ten clear left">' + message + '</p>' + button + '</div></div>';
                        //added appointment
                        $('#ajaxResult').html(html);
                    } else {
                        if (res == 0) {
                            alert('not cancelled!');
                        }
                    }
                }
            });
        }
}

function changeMin() {
    var hour = document.getElementById("hour");
    var selectedValue = hour.options[hour.selectedIndex].value;
    if(selectedValue == 9) {
        document.getElementById('minute').innerHTML = '<option value="30">30</option><option value="45">45</option>';

    } else {
        document.getElementById('minute').innerHTML = '<option value="00">00</option>' +
                                                 '<option value="15">15</option>' +
                                                 '<option value="30">30</option>' +
                                                 '<option value="45">45</option>';

    }
}






