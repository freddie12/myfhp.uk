<?php

class Venue {

    private $id, $buildingName, $firstLineOfAddress, $town, $county, $postcode;

    function __construct($id, $buildingName, $firstLineOfAddress, $town, $county, $postcode) {
        $this->setId($id);
        $this->setBuildingName($buildingName);
        $this->setFirstLine($firstLineOfAddress);
        $this->setTown($town);
        $this->setCounty($county);
        $this->setPostcode($postcode);
    }

    function setId($id) {
        $this->id = $id;
    }

    function setBuildingName($buildingName) {
        $this->buildingName = $buildingName;
    }

    function setFirstLine($b) {
        $this->firstLineOfAddress= $b ;
    }

    function setTown($b) {
        $this->town = $b ;
    }

    function setCounty($b) {
        $this->county = $b ;
    }

    function setPostcode($b) {
        $this->postcode = $b ;
    }

    function getId() {
        return $this->id ;
    }

    function getBuildingName() {
        return $this->buildingName;
    }

    function getFirstLine() {
        return $this->firstLineOfAddress;
    }

    function getTown() {
        return $this->town;
    }

    function getCounty() {
        return $this->county;
    }

    function getPostcode() {
        return $this->postcode;
    }
}