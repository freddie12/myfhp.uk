<?php

class Appointment {

    public $id;
    public $sDate;
    public $eDate;
    public $start;
    public $end;
    public $duration;
    public $staff;
    public $staffId;
    public $user;
    public $notes;
    public $attended;
    public $userfName;
    public $userlName;
    public $venue;
    public $confirmed;
    public $break;
    public $reminder_sent;

    public function __construct($id, $sDate, $eDate, $user, $userfName, $userlName, $staff, $notes, $attended, $venueId = '', $staffId = '', $confirmed = '', $break = '', $reminder_sent = '') {
        $this->id = $id;
        $this->sDate = $sDate;
        $this->eDate = $eDate;
        $this->formatDateTime();
        $this->user = $user;
        $this->userfName = $userfName;
        $this->userlName = $userlName;
        $this->staff = $staff;
        $this->notes = $notes;
        $this->attended = $attended;
        $this->staffId = $staffId;
        $this->confirmed = $confirmed;
        $this->break = $break;
        $this->reminder_sent = $reminder_sent;

        (!empty($venueId) ? $this->setVenue($venueId) : '');
    }

    private function setVenue($id) {
        $this->venue = Functions::getVenue($id);

    }

    private function formatDateTime() {
        $this->start = Functions::getTime($this->sDate);
        $this->end   = Functions::getTime($this->eDate);
    }

    public function compareTo() {
        //if this->date is further then object return 1;
        //if this->date is closer than object return 0;
        //if same date return -1;
    }

}

?>