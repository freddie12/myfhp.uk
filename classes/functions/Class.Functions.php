<?php

class Functions {



    public static function readableDate($date) {
        return date("d.m.Y", strtotime(str_replace("-", "/", $date)));
    }

    public static function getTime($date) {
        return date("H:i", strtotime($date));
    }

    public static function dateAsStr($date) {
        return date("l jS M, Y", strtotime($date));
    }

    public static function unixToTimestamp($unix) {
        return date("Y-m-d H:i:s", $unix);
    }


    public static function updatePhone($number, $id) {
        global $db;
        $query = "  UPDATE
                            client_user
                        SET
                            phone = '" . $number . "'
                        WHERE
                            id = '" . $id . "';
                     ";

        if($db->quickQuery($query))
            return 1;
        else
            return 0;
    }


    /**
     * @param $dates
     * @return mixed
     *
     * Pass an array of timestamps
     *
     * Get the index of that array of the next date to arrive in the future
     *
     */

    public static function nextDate($dates) {
        //TASK: figure out which date is closest to now
        $index = 0;
        $futureAppointment = array();
        $counter = 0;
        //get all objects with dates in the future
        //if timestamp in seconds is greater than current time then it hasn't arrived yet.
        foreach($dates as $date) {
            if(Functions::isInFuture($date)) array_push($futureAppointment, $counter);
            $counter++;
        }

        if(sizeof($futureAppointment) == 0) return -1;//no future appointments avalailable

         $counter = 1;

          while($counter < sizeof($futureAppointment)) {
               //if index value isGreater then counter value then counter value must be a closer date
               if(Functions::timeDifference($dates[$futureAppointment[$index]])
                  >
                  Functions::timeDifference($dates[$futureAppointment[$counter]])) {
                  $index=$counter;
               }
               $counter++;
          }

          return $futureAppointment[$index];//return the index value
    }

    public static function deleteAppointment($key) {
        global $db;
        $query = "
                        DELETE FROM
                          appointment
                        WHERE
                          validation_key = '" . $key . "';
                     ";
        if($db->quickQuery($query)) return 1;
        else return 0;
    }

    public static function timeDifference($date) {
        //take time in seconds and return the difference between now and then
        $difference = strtotime($date)-time();
        return $difference;
    }

    public static function isInFuture($date) {
        //convert currTime to seconds
        $appTime = strtotime($date);
        if(time() < $appTime) return true;
        else return false;
    }

    public static function isUser($id) {
        global $db;
        $query = "SELECT
                    *
                  FROM
                    client_user
                  WHERE
                    id = '" . $id . "'
                 ";
        if($db->fetchOneRow($query)) return 1;
        else return 0;
    }

    public static function isEmail($email) {
        global $db;
        $secureEmail = Functions::escapeInjection($email);
        $query = "SELECT
                    *
                  FROM
                    client_user
                  WHERE
                    email = '" . $secureEmail . "'
                 ";
        if($db->fetchOneRow($query)) return 1;
        else return 0;
    }
    
    public static function liveAccount($email) {
       global $db;
       $secureEmail = Functions::escapeInjection($email);
       $query = "SELECT
                    *
                  FROM
                    client_user
                  WHERE
                    email = '" . $secureEmail . "'
                  AND
                    verified = '1';
                 ";
       if($db->fetchOneRow($query)) return 1;
       else return 0;
   }

    public static function latestIdEntry() {
        global $db;
        $query = "SELECT
                    MAX(id) as id
                  FROM
                    client_user
                 ";
        $res = $db->fetchOneRow($query);
        return $res->id;
    }

    public function getEndTime($startDate, $endDate) {

    }
    
    public static function deleteUser($key) {
        global $db;
        

        $query = "DELETE FROM
                    client_user
                  WHERE
                    id = '" . $key . "'";
        
        $db->quickQuery($query);
    }

    public static function latestRecipe()
    {
        global $db;
        $query = "SELECT
                    MAX(id) as id
                  FROM
                    recipe
                 ";
        $res = $db->fetchOneRow($query);
        return $res->id;
    }

    public static function isRecipeSaved($uId, $rId) {
        global $db;
        $query = "  SELECT
                        ur.id
                    FROM
                        client_user cu, recipe r, user_recipe ur
                    WHERE
                        r.id = ur.recipe
                    AND
                        ur.client_user = cu.id
                    AND
                        cu.id = '" . $uId . "'
                    AND
                        r.id = '" . $rId . "'
                    ORDER BY title ASC;
                 ";
        if($db->fetchOneRow($query)) {
            return 1;
        } else {
            return 0;
        }

    }

    public static function saveRecipe($user, $recipe) {
        global $db;
        $query = "INSERT INTO
                    user_recipe (client_user, recipe)
                  VALUES
                    (
                      '" . $user . "',
                      '" . $recipe . "'
                    )
                 ";
        $res = $db->quickQuery($query);
        return $res;
    }

    public static function removeRecipe($user, $recipe) {
        global $db;
        $query = "DELETE FROM
                    user_recipe
                  WHERE
                    client_user = '" . $user  . "'
                  AND
                    recipe = '" . $recipe ."'
                 ";
        $res = $db->quickQuery($query);
        return $res;
    }

    public static function addRecipe($t, $d, $m, $i) {
        global $db;
        $query = "INSERT INTO
                    recipe (title, description, method, ingredients)
                  VALUES
                    (
                      '" . $t . "',
                      '" . $d . "',
                      '" . $m . "',
                      '" . $i . "'
                    )
                 ";
        $res = $db->quickQuery($query);
        return $res;
    }

    public static function editRecipe($t, $d, $m, $i, $id) {
        global $db;

        $query = "UPDATE
                    recipe
                    SET
                       title        =   '" . $t . "',
                       description  =   '" . $d . "',
                       method       =   '" . $m . "',
                       ingredients  =   '" . $i . "'
                    WHERE
                       id = '" . $id . "'
                 ";
        $res = $db->quickQuery($query);
        return $res;
    }

    public static function deleteRecipe($i) {
        global $db;
        $query = "DELETE FROM
                    recipe
                  WHERE
                    id = '" . $i . "';

                 ";
        $res = $db->quickQuery($query);
        return $res;
    }


    public static function getRecipeById($id) {
        global $db;

        $query = "  SELECT
                         *
                    FROM
                        recipe
                    WHERE
                        id = '" . $id . "'
                 ";

        $result = $db->fetchOneRow($query);
        $recipe = new Recipe(
                    $result->id,
                    $result->title,
                    $result->description,
                    $result->method,
                    $result->ingredients
                   );
        for($i=0;$i<1;$i++) {}

        return $recipe;
    }

    public static function getRecipeBySearchName($string, $condition = array()) {
        global $db;
        $recipeList = array();

        $query = "  SELECT
                         r.id, r.title, r.description, r.ingredients, r.method
                    FROM
                        recipe r";

        if(!empty($condition)) {
            $query .= ", recipe_health_condition rhc";
        }


        $query .=    "
                        WHERE
                            r.title like '%" . $string . "%'
                        OR
                            r.ingredients like '%" . $string . "%'
                     ";

        /**
         * if the search is provided health
         * conditions then add them onto the query
         */
        if(!empty($condition)) {
            $query .= "
                        AND
                            r.id = rhc.recipe
                        AND
                            rhc.health_condition IN ('";
            for($i=0;$i<sizeof($condition);$i++) {
                $query .= $condition[$i] . "'";
                if(! ($i+1 == sizeof($condition)) ) {
                    $query .= ",";
                }
            }
            $query .= ")";
        }

        $query .= "
                    ORDER BY title ASC
                  ";




        foreach($db->iterate($query) as $recipe) {
            array_push($recipeList, new Recipe(
                $recipe->id,
                $recipe->title,
                $recipe->description,
                $recipe->method,
                $recipe->ingredients
            ));
        };

        return $recipeList;
    }

    public static function convertToHtml($string) {
        return str_replace(array("\n"), '</br>', $string);
    }

    public static function convertToTextArea($string) {
        return str_replace(array("</br>"), "\n", $string);
    }

    public static function getAllRecipe($sort = '') {
        global $db;
        $recipeList = array();

        $query = "  SELECT
                         r.id, r.title, r.description, r.ingredients, r.method
                    FROM
                        recipe r
                    WHERE
                        title like '" . $sort . "%'
                    ORDER BY title ASC;
                 ";

        foreach($db->iterate($query) as $recipe) {
            array_push($recipeList, new Recipe(
                $recipe->id,
                $recipe->title,
                $recipe->description,
                $recipe->method,
                $recipe->ingredients
            ));
        };

        return $recipeList;
    }

    public static function getAllSavedRecipe($id) {
        global $db;
        $recipeList = array();

        $query = "  SELECT
                        r.id, r.title, r.description, r.ingredients, r.method
                    FROM
                        client_user cu, recipe r, user_recipe ur
                    WHERE
                        r.id = ur.recipe
                    AND
                        ur.client_user = cu.id
                    AND
                        cu.id = '" . $id . "'

                    ORDER BY title ASC;
                 ";

        foreach($db->iterate($query) as $recipe) {
            array_push($recipeList, new Recipe(
                $recipe->id,
                $recipe->title,
                $recipe->description,
                $recipe->method,
                $recipe->ingredients
            ));
        };

        return $recipeList;
    }

    public static function getAlphabet($array) {
        $alphabet = array();
        $letters = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q','R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

        foreach($letters as $let) {
            foreach($array as $arr) {
                if(strtoupper($arr[0]) == $let) {
                    array_push($alphabet, $let);
                    break;
                }

            }
        }
        return $alphabet;
    }

    public static function listUsers($order = '') {
        global $db;

        $user = array();

        $query = "SELECT
                    *
                  FROM
                    client_user
                  WHERE
                    admin != '1'";

        if(!empty($order)) $query .= "ORDER BY " . $order;

        foreach ($db->iterate($query) as $row) {
            array_push($user, new ClientUser($row->id));
        }

        return $user;
    }

    public static function unknownUser(ClientUser $user) {
        if($user->getfName() == null ) return true;
    }

    public static function badUrl($view, $model) {
            $view->setBody('error');
            $view->setLayout('main');
            $model->setContent('error', 'You have entered a bad URL');
    }

    public static function dateClash($timestamp1, $timestamp2) {

        global $db;

        $query = "SELECT
                    *
                  FROM
                    appointment
                  WHERE
                    start <= TIMESTAMP('" .$timestamp1. "')
                  AND
                    end   BETWEEN TIMESTAMP('" .$timestamp1. "') AND TIMESTAMP('" .$timestamp2. "')

                  OR
                    start <= TIMESTAMP('" .$timestamp1. "')
                  AND
                    end   >= TIMESTAMP('" .$timestamp2. "')

                  OR
                    start BETWEEN TIMESTAMP('" .$timestamp1. "') AND TIMESTAMP('" .$timestamp2. "')
                  AND
                    end   >= TIMESTAMP('" .$timestamp2. "');
                   ";

        if($db->fetchOneRow($query)) {
            return 1;
        } else {
            return 0;
        }
    }

    public static function getVenue($id) {
        global $db;
        $query = "SELECT
                    *
                  FROM
                    venue
                  WHERE
                    id = '" . $id ."'
                  ";
        $res = $db->fetchOneRow($query);
        $venObject = new Venue(
            $res->id,
            $res->buildingName,
            $res->firstLineOfAddress,
            $res->town,
            $res->county,
            $res->postcode
        );
        return $venObject;
    }

    public static function getAppointmentById($id) {
        global $db;
        $query = "SELECT
                        *
                    FROM
                        appointment
                    WHERE
                        id= '" . $id . "'
                 ";
        if($res = $db->fetchOneRow($query)) return $res;
        else return 0;

    }

    public static function getAppointmentByDate($date) {
        global $db;
        $appointments = array();
        $query = "SELECT
                        a.id as 'id',
                        a.start,
                        a.end,
                        a.notes,
                        a.attended,
                        a.break,
                        a.reminder_sent,
                        a.confirmed,
                        s.name as 'staffName',
                        s.id as 'staffId',
                        cu.id as 'client_user',
                        cu.fName as 'userfName',
                        cu.lName as 'userlName',
                        cu.id as 'client_id',
                        v.id as venue
                    FROM
                        appointment a,
                        client_user cu,
                        staff s,
                        venue v
                    WHERE
                        a.client_user = cu.id
                    AND
                        a.venue = v.id
                    AND
                    	s.id = a.staff
                    AND
                        a.start like '" . $date . "%'
                    ORDER BY
                        start ASC";
        foreach($db->iterate($query) as $row) {
            array_push($appointments, new Appointment
            (
                $row->id,
                $row->start,
                $row->end,
                $row->client_user,
                $row->userfName,
                $row->userlName,
                $row->staffName,
                $row->notes,
                $row->attended,
                $row->venue,
                $row->staffId,
                $row->confirmed,
                $row->break,
                $row->reminder_sent
            ));
        }
        $size = sizeof($appointments);
        if($size > 0) return $appointments;
        else return 0;

    }

    public static function getAppointmentsByMonth(User $user, $timestamp1 = '', $timestamp2 = '') {
        global $db;

        $appointments = array();

        if(empty($timestamp1)) $timestamp1 = date("Y-m-01 00:00:00", strtotime('now'));
        if(empty($timestamp2)) $timestamp2 = date("Y-m-01 00:00:00", strtotime('+1 month', strtotime('now')));

        $query = "SELECT
                        a.id as 'id',
                        a.start,
                        a.end,
                        a.notes,
                        a.attended,
                        s.name as 'staffName',
                        cu.fName as 'userfName',
                        cu.lName as 'userlName',
                        cu.id as 'client_id'
                    FROM
                        appointment a,
                        client_user cu,
                        staff s
                    WHERE
                        cu.id = a.client_user
                        AND
                        s.id = a.staff
                        AND
                        client_user = '" .$user->getId(). "'
                        AND
                        start BETWEEN TIMESTAMP('" .$timestamp1. "') AND TIMESTAMP('" .$timestamp2. "')
                        AND
                        confirmed = '1'
                    ORDER BY
                        start ASC";

        foreach ($db->iterate($query) as $row) {
            array_push($appointments, $app = new Appointment
            (
                $row->id,
                $row->start,
                $row->end,
                $row->client_id,
                $row->userfName,
                $row->userlName,
                $row->staffName,
                $row->notes,
                $row->attended
            ));
        }

        return $appointments;
    }

    public static function getUserIdByEmail($email) {
        global $db;
        $query = "SELECT
                    id
                  FROM
                      client_user
                  WHERE
                      email = '" . $email . "';
                 ";
        $res = $db->fetchOneRow($query);
        return $res->id;
    }

    public static function getStaff($id) {
        global $db;
        $query = "
                    SELECT
                        *
                    FROM
                        staff
                    WHERE
                        id = '" . $id . "';
                 ";
        return $db->fetchOneRow($query);
    }

    public static function createRandomKey($table) {
        global $db;
        //create random, unique code that can identify the user
        do{
            //create random string
            $rs = Functions::randomString();
            //check if string is used already
            $data = Functions::checkKey($rs, $table);
        } while($data);//wait for a return of 0 from db indicating a unique key
        return $rs;
    }

    public static function randomString() {
        $length = 20;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = '';

        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }

        return $randomString;
    }

    public static function checkKey($rs, $table) {
        global $db;
        $rs = Functions::escapeInjection($rs);

        $query="SELECT
                    validation_key
                FROM
                    " . $table . "
                WHERE
                    validation_key ='" . $rs . "'
                ";
        $result = $db->fetchOneRow($query);
        return $result;
    }

    public static function escapeInjection($injection) {
        if($injection = mysql_real_escape_string($injection)) {
            return $injection;
        } else {
            return 0;
        }
    }

    public static function changePassword($password, $column, $key) {
        global $db;

        if($password = Functions::escapeInjection($password)) {
            $password = sha1($password);

            $query="UPDATE
                        client_user
                    SET
                        password = '" . $password . "'
                    WHERE
                        " . $column . " = '" . $key . "';
                    ";
            if($db->quickQuery($query)) return 1;
            else return 0;
        } else {
            return 0;
        }
    }

    public static function updateEmail($key) {
        global $db;
        $query = "SELECT
                       newEmail
                      FROM
                       client_user
                      WHERE
                       validation_key = '" . $key . "'
                       ";

        $res = $db->fetchOneRow($query);

        $newEmail = $res->newEmail;

        $query = "
                    UPDATE
                        client_user
                    SET
                        email = '" . $newEmail . "',
                        newEmail = '',
                        validation_key = ''
                    WHERE
                        validation_key = '" . $key . "';
                 ";
        if($db->quickQuery($query)) return 1;
        else return 0;
    }

    public static function deleteKey($key) {
        global $db;

        $query="UPDATE
                    client_user
                SET
                    validation_key = ''
                WHERE
                    validation_key = '" . $key . "';
                ";
        if($db->quickQuery($query)) return 1;
    }


}