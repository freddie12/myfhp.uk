<?php

    class AccountModel extends Model {

        function init() {
            $this->setContent('warning', '');

        }

        function setNextAppointmentIndex() {
            $this->setContent('nextAppointmentIndex', $this->userObj->getNextAppointmentIndex());
        }

        function createNextAppointmentMessage($index) {
            //Is a future date
            if($index >= 0 && $this->userObj->getAppointments()[$index]->confirmed) {
                return $this->userObj->createAppointmentMessage($this->content['nextAppointmentIndex']);
            } else {
                //set spec mess
                return $this->userObj->getDefaultAppointmentMessage();
            }
        }

        function deleteAppointment($key) {
            return Functions::deleteAppointment($key);
        }

        function confirmAppointment($key) {
            global $db;
            if($this->sendAppointmentRequestConfirmationEmail($key)) {
                //set database
                $query = "UPDATE
                            appointment
                          SET
                            validation_key = '', confirmed = '1'
                          WHERE
                            validation_key = '" . $key ."';
                            ";
                if($db->quickQuery($query))return 1;
                else return 0;
            } else {
                return 0;
            }
        }

        function sendAppointmentRequestConfirmationEmail($key) {
            $query = "  SELECT
                            *
                        FROM
                            appointment
                        WHERE
                            validation_key = '" . $key . "';
                      ";
            //get appointment
            if($res = $this->db->fetchOneRow($query)) {
                //store user
                $user = new ClientUser($res->client_user);
                $emailUserEmail                            = $user->getEmail();
                $emailUserName                             = $user->getFullName();

                //store appointment
                $appointment = new Appointment($res->id,
                                                $res->start,
                                                $res->end,
                                                $user->getId(),
                                                $user->getfName(),
                                                $user->getlName(),
                                                $res->staff,
                                                $res->notes,
                                                $res->attended,
                                                $res->venue
                                                );
                $emailTimeFrom = date("H:i", strtotime($appointment->start));
                $emailTimeTo   = date("H:i", (strtotime($appointment->end)+60));

                $emailDate = Functions::dateAsStr($appointment->start);

                //store venue details
                $emailAddressBuildingName                   = $appointment->venue->getBuildingName();
                $emailAddressFirstLineOfAddress             = $appointment->venue->getFirstLine();
                $emailAddressTown                           = $appointment->venue->getTown();
                $emailAddressCounty                         = $appointment->venue->getCounty();
                $emailAddressPostcode                       = $appointment->venue->getPostcode();

                //store staff details
                $staff = Functions::getStaff($appointment->staff);
                $emailStaffName     =   $staff->name;
                $emailStaffPhone    =   $staff->phone;
                $emailStaffEmail    =   $staff->email;
                $emailStaffEmailLink = 'mailto:' . $emailStaffEmail;
                global $sendMail;
                require_once(HTML . 'email.sendAppointmentRequestConfirmation.html');
                if($sendMail->carrierPigeon($user->getEmail(), 'Appointment Confirmation', $content, 'no-reply')) return 1;
                else return 0;
            } else {
                return 0;
            }
        }



        function passwordChangeSubmit() {
            if(isset($_POST['oldPassword']) && !empty($_POST['oldPassword'])
                && isset($_POST['newPassword']) && !empty($_POST['newPassword'])
                && isset($_POST['newPasswordConfirmation']) && !empty($_POST['newPasswordConfirmation']))
            {
                if($this->checkOldPassword($_POST['oldPassword'])) {
                    if($_POST['newPassword'] == $_POST['newPasswordConfirmation']) {
                        if(Functions::changePassword($_POST['newPassword'], 'id', $this->userObj->getId())) {
                            $this->setContent('confirmation-header', 'Well done!');
                            $this->setContent('confirmation-message', 'Password changed succesfully!');
                            return 1;
                        } else {
                            $this->setContent('warning', 'Sorry, something has gone wrong. Try again');
                            return 0;
                        }
                    } else {
                        $this->setContent('warning', 'Your new password doesn\'t match.');
                    }
                } else {
                    $this->setContent('warning', 'Your password is wrong');
                    return 0;
                }
            } else {
                $this->setContent('warning', 'Please fill every field.');
                return 0;
            }
        }

        function checkOldPassword($password) {

            $password = sha1(Functions::escapeInjection($password));

            $id       = $this->userObj->getId();
            $query = "SELECT
                        password
                      FROM
                        client_user
                      WHERE
                        password = '" . $password . "'
                      AND
                        id = '" . $id . "';
                     ";

            if($this->db->fetchOneRow($query)) return 1;
            else return 0;
        }



        function setUserDetails() {
            //set personal content
            $this->setContent('name', $this->userObj->getFullName());
            $this->setContent('email', $this->userObj->getEmail());
            $this->setContent('dob', Functions::readableDate($this->userObj->getDob()));
            $this->setContent('phone', $this->userObj->getPhone());
        }

        function getContent(){}
    }
?>