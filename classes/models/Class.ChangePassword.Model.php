<?php



    class ChangePasswordModel extends Model {

        function init() {
            $this->setContent('email-warning', '');
            $this->setContent('password-warning', '');
        }

        function getContent(){}

        function emailLinkPressed() {
            if(isset($_GET['key']) && !empty($_GET['key'])) {
                if(Functions::checkKey($_GET['key'], 'client_user')) {
                    if($this->changePassword()) return 1;
                    else return 0;
                } else {
                    return 'badurl';
                }
            } else {
                return 'badurl';
            }
        }

        /**
         * Allow the user to change the password using their unique key
         */
        function changePassword() {
            if(isset($_POST['submit'])) { //submit pressed
                if(isset($_POST['confirmPassword']) && !empty($_POST['confirmPassword']) && isset($_POST['password']) && !empty($_POST['password'])) { // password supplied
                    if($_POST['password'] == $_POST['confirmPassword']) {
                        if(Functions::changePassword($_POST['password'], 'validation_key', $_GET['key'])) {
                            Functions::deleteKey($_GET['key']);
                            $this->setContent('confirmation-header', 'Thank you!');
                            $this->setContent('confirmation-message', 'Your password has been changed successfully');
                            return 1;
                        } else {
                            $this->setContent('password-warning', 'Your password doesn\'t seem to agree with our system.');
                        }
                    } else $this->setContent('password-warning', 'Passwords did not match.');
                } else $this->setContent('password-warning', 'Please enter both fields.');
            }

            //if successful then this will not be reached because of the return 1
            $this->setContent('header', 'Reset Password');
            $this->setContent('subheader', 'Please enter your new password below..');
            return 0;

        }
        
        function sendReset($email){
            $key = Functions::createRandomKey('client_user');
            
            $query = "UPDATE 
                        client_user
                      SET 
                        validation_key = '" . $key ."'
                      WHERE 
                        email = '" . $email . "'";
            
            if($this->db->quickQuery($query)){

                //store user datails
                $user = new ClientUser(Functions::getUserIdByEmail(Functions::escapeInjection($email)));
                $emailUserName = $user->getFullName();
                //store link
                $emailLinkA = THIS_DOMAIN . 'forgotpassword/?action=changepassword&key=' . $key;
                require_once(HTML . 'email.forgotpassword.html');
                global $sendMail;
                if ($sendMail->carrierPigeon($email,
                                              'Password Reset',
                                              $html,
                                              'no-reply')){
                    $this->setContent('confirmation-header', 'Thank you');
                    $this->setContent('confirmation-message', 'please check your email to reset your password');
                } else {
                    $this->setContent('confirmation-header', 'Not Sent');
                    $this->setContent('confirmation-message', 'Email Error. Please Retry');
                }                
            } else {
                $this->setContent('confirmation-header', 'Not Sent');
                $this->setContent('confirmation-message', 'Database Error. Please Retry');
            }
        }


    }
?>