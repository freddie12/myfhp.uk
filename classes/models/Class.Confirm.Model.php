<?php

class ConfirmModel extends Model {
    private $error;
    private $defaultMessage;
        
    function getContent() {
        //nothing to initiallise
    }





    function init()
    {
        $this->setError(0);
        $this->setDefaultMessage('');
        if (isset($_GET['action']) && !empty($_GET['action'])) {
            switch ($_GET['action']) {
                case 'emailchange':
                    if (isset($_GET['key']) && !empty($_GET['key']))
                    {
                        //continue with displaying the recipe
                        $key = Functions::escapeInjection($_GET['key']);
                        if(Functions::checkKey($key, 'client_user')) {
                            $this->updateEmail($key);
                            return 1;
                        } else {
                            return 0;
                        }

                    } else {
                        $this->setError('Error');
                        return 0;
                    }
                    break;


                default:
                    break;
            }
        }  else {
            $this->setError('Error');
            return 0;
        }
    }

    function updateEmail($key) {
        if(Functions::updateEmail($key)) return 1;
        else return 0;
    }

    function setError($error) {
        $this->error= $error;
    }


    function getError() {
        return $this->error;
    }

    function setDefaultMessage($message) {
        $this->defaultMessage = $message;
    }

    function getDefaultMessage() {
        return $this->defaultMessage;
    }






}
?>