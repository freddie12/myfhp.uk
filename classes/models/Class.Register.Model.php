<?php

class RegisterModel extends Model {

    public $details = array();
        
    function getContent() {
        //nothing to initiallise
    }

    function validateForm() {
        $this->resetWarning();

        //get post vars
        $this->setFieldText();

        if($this->validate()) {
            return 1;
        }
        else  {
            return 0;
        }
    }





    function register() {

        $password = sha1($this->details['password']);
        $key = Functions::createRandomKey('client_user');
        $query = "
                INSERT INTO
                    client_user (email, fName, lName, dob, password, validation_key, newsletter, phone)
                VALUES (
                            '". $this->details['email'] ."',
                            '". $this->details['fname'] ."',
                            '". $this->details['sname'] ."',
                            '1986-03-04',
                            '" . $password . "',
                            '" .$key. "',
                            '" . $this->details['checkbox'] . "',
                            '" . $this->details['phone'] . "'
                       )

                 ";
        if($this->db->quickQuery($query)) {
            $emailUserName = $this->details['fname'] .  ' ' . $this->details['sname'];
            $emailLinkA = THIS_DOMAIN . 'register?i=' . $key . '&u=' . Functions::latestIdEntry();

            require_once(HTML . 'email.registration.html');
            global $sendMail;
            if($sendMail->carrierPigeon($this->details['email'],
                                        'Registration',
                                         $html,
                                        'register'))
            {
                $this->setContent('confirmation-header', 'Almost done..');
                $this->setContent('confirmation-message', 'Please check your email to validate your account.');
            } else {
                $this->setContent('confirmation-header', 'Not sent');
                $this->setContent('confirmation-message', 'Email Error. Please retry');
                Functions::deleteUser(Functions::latestIdEntry());
            }
        } else {
            $this->setContent('confirmation-header', 'Not sent');
            $this->setContent('confirmation-message', 'Database Error. Please retry');
        }
    }







    function setFieldText() {
        if(isset($_POST['fname']))              $this->details['fname'] = ucwords(strtolower($_POST['fname']));
        if(isset($_POST['sname']))              $this->details['sname'] = ucwords(strtolower($_POST['sname']));
        if(isset($_POST['email']))              $this->details['email'] = $_POST['email'];
        if(isset($_POST['confirm-email']))      $this->details['confirm-email'] = $_POST['confirm-email'];
        if(isset($_POST['password']))           $this->details['password'] = $_POST['password'];
        if(isset($_POST['confirm-password']))   $this->details['confirm-password'] = $_POST['confirm-password'];
        if(isset($_POST['phone']))              $this->details['phone'] = $_POST['phone'];

        if(isset($_POST['checkbox']))           $this->details['checkbox'] = 1;
        else                                    $this->details['checkbox'] = 0;

    }

    function validate() {
        if(empty($this->details['fname']))              $this->setContent('fname-warning', '* Please enter a forename');
        else                                            $this->formatName('fname');

        if(empty($this->details['sname']))              $this->setContent('sname-warning', '* Please enter a surname');
        else                                            $this->formatName('sname');

        if(empty($this->details['email']))              $this->setContent('email-warning', '* Please enter a email');
        else                                            $this->formatEmail('email');

        if(empty($this->details['confirm-email']))      $this->setContent('confirm-email-warning', '* Please confirm your email');
        else                                            $this->formatEmail('email');

        if(empty($this->details['password']))           $this->setContent('password-warning', '* Please enter a password');
        if(empty($this->details['confirm-password']))   $this->setContent('confirm-password-warning', '* Please confirm your password');

        if(empty($this->details['phone']))              $this->setContent('phone-warning', '* Please enter a phone number');
        else                                            $this->isValidPhoneNumber();

        if(!$this->compare($this->details['email'],
            $this->details['confirm-email'])
          )                                             $this->setContent('confirm-email-warning', '* email does not match');

        if(!$this->compare($this->details['password'],
            $this->details['confirm-password'])
          )                                             $this->setContent('confirm-password-warning', '* password does not match');

        if(!$this->isUniqueEmail()) {
                                                        $this->setContent('email-warning', '* email already in use');
                                                        $this->details['confirm-email'] = '';
        };

        foreach($this->content as $deets) {
            if(!empty($deets)) return false;
        }

        return true;
    }

    function resetWarning() {
        $this->setContent('fname-warning', '');
        $this->setContent('sname-warning', '');
        $this->setContent('email-warning', '');
        $this->setContent('confirm-email-warning', '');
        $this->setContent('password-warning', '');
        $this->setContent('confirm-password-warning', '');
        $this->setContent('phone-warning', '');
    }

    function formatName($name) {
        $pattern = '/^[ A-Za-z]+$/';
        $this->details[$name] = trim(mysql_real_escape_string($this->details[$name]));

        if(preg_match($pattern, $this->details[$name])) {
            return true;
        } else {
            $this->setContent($name.'-warning', '* name can only be letter a-z');
        }
    }

    function formatEmail($name) {
        if(filter_var($this->details[$name], FILTER_VALIDATE_EMAIL)) {
            return true;
        } else {
            $this->setContent($name.'-warning', '* not a valid email');
        }
    }

    function isValidPhoneNumber() {
        if(!is_numeric($this->details['phone'])) $this->setContent('phone-warning', '* this is not a valid number');
        else return true;
    }

    function compare($a, $b) {
        if($a == $b) return true;
        else return false;
    }

    function isUniqueEmail() {
        $query = "SELECT
                    id
                  FROM
                    client_user
                  WHERE
                    email = '" . $this->details['email'] . "';
                 ";

        if(!$this->db->fetchOneRow($query)) return true;
        else return false;
    }

    function checkUserKey($key, $id) {
        $query = "
                 SELECT
                    validation_key
                 FROM
                    client_user
                 WHERE
                    validation_key = '". $key ."'
                 ";

        $row = $this->db->fetchOneRow($query);

        if(!$row) {
            if(Functions::isUser($id)) {
                if($this->verified($id)) {
                    $this->setContent('confirmation-header', 'Oops!');
                    $this->setContent('confirmation-message', 'Already verified');
                    return false;
                } else {
                    $this->setContent('confirmation-message', 'Validation Error. Contact Administrator!');
                    return false;
                }

            } else {
                $this->setContent('confirmation-header', 'Oops!');
                $this->setContent('confirmation-message', 'No user found');
                return false;
            }
        }

        else {
            $this->key = $row->validation_key;
            return true;
        }
    }

    function verified($id) {
            $query = "SELECT
                        verified
                      FROM
                        client_user
                      WHERE
                        id = '" . $id . "'
                     ";
            $i = $this->db->fetchOneRow($query);

            if($i->verified) return 1;
            else return 0;
    }

    function validateUser() {
        if(!empty($this->key)) {
            $query = "UPDATE
                        client_user
                      SET
                        verified = 1, validation_key = ''
                      WHERE
                        validation_key = '" . $this->key . "'
                     ";
            if($this->db->query($query)) {
                Functions::deleteKey($this->key);
                $this->setContent('confirmation-header', 'Complete!');
                $this->setContent('confirmation-message', 'You can now <a href="javascript:;" class="red" onclick="displayLogin()">login</a> to view your account');
                return 1;
            }
            else return 0;
        } else {
            return 0;
        }
    }


}
?>