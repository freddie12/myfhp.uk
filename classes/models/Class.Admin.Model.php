<?php
class AdminModel extends Model {


    private $isFilled = false;
    private $appointments = array();




    function setUserArray($userArray) {

        $this->setContent('user', array());

        foreach($userArray as $user) {
            array_push($this->content['user'], array(
                    'id'        =>  $user->getId(),
                    'email'     =>  $user->getEmail(),
                    'fName'     =>  $user->getfName(),
                    'lName'     =>  $user->getlName(),
                    'dob'       =>  $user->getDob(),
                    'verified'  =>  $user->getVerified(),
                    'newsletter'=>  $user->getNewsletter(),
                    'phone'     =>  $user->getPhone()
                )
            );

        }
    }

    function setEditRecipe() {
        $tempStr = $this->recipes->getIngredients()[0];
        $limit = sizeof($this->recipes->getIngredients());
        for($i=1;$i<$limit;$i++) {
            $tempStr .= '&#13;';
            $tempStr .= $this->recipes->getIngredients()[$i];
        }

        $this->recipes->setIngredients($tempStr);
    }

    function setAppointmentByDate($date) {
        $appointments = Functions::getAppointmentByDate(Functions::escapeInjection($date));
        if($appointments) {
            //do something with dates
            $this->appointments = $appointments;
            $this->isFilled = true;
        } else {
            $this->isFilled = false;
        }
    }

    function setDate($date) {
        $this->setContent('date', Functions::dateAsStr($date));
    }

    function isFilled() {
        return $this->isFilled;
    }

    function getAppointment() {
        return $this->appointments;
    }

    function setAllRecipes() {
        $this->recipes = Functions::getAllRecipe();
    }


    function setSingleRecipe($id) {
        $this->recipes = Functions::getRecipeById($id);
    }

    function getContent() {}




}
?>