<?php

class RecipeModel extends Model {
    private $sortLetter;
    private $nextPageLetter;
    public  $details = array();
    private $pageCount;
    private $pageLimit;
    private $nextPage;
    private $listStartAt;
    private $numPerPage =  5;
    private $hasNextPage = 1;
    private $searchName;
    private $error;
    private $nextPageHref;
    private $previousPage = 0;
    private $defaultMessage;
    private $search = 0;
    private $view;
    private $recipeId;
    private $saved;
        
    function getContent() {
        //nothing to initiallise
    }





    function init() {
        $this->setError(0);
        $this->setDefaultMessage('');
        switch($_GET['action']) {
            case 'singlerecipe':
                if(isset($_GET['action']) && !empty($_GET['action']) && isset($_GET['id']) && !empty($_GET['id'])) {
                    //continue with displaying the recipe
                    $this->setRecipeId(Functions::escapeInjection($_GET['id']));
                    $this->setRecipe(Functions::escapeInjection($_GET['action']));
                    $this->convertStringToViewIn('<p>');
                    $this->setIsSaved();
                    $this->setView('singlerecipe');
                    return 1;
                } else {
                    $this->setError('Error');
                    return 0;
                }
                break;

            case 'myrecipe':
                //default a-z
                //set up array to print a-z
                if(LOGGED_IN) {
                    $this->setContent('header', 'My Recipes');
                    $this->setAlphabettiSpaghetti('myrecipe');
                    //set up array of recipes
                    $this->setRecipe('myrecipe');
                    //save sorting letter
                    $this->setSortLetter();
                    //save the letter that will be called next in the pages
                    $this->nextPageLetter = $this->getSortLetter();
                    //throw error if url is broken
                    if (isset($_GET['page']) && !empty($_GET['page'])) $this->setPageCount($_GET['page']);
                    else if (isset($_GET['page']) && empty($_GET['page'])) return 0;
                    else $this->setPageCount(1);
                    //set up limits for the html
                    if (!$this->noRecipe()) {
                        $this->setPageLimits();
                        $this->setContent('subheader', 'Here are all your favourite recipes.');
                        $this->setDirectionalHref('default');
                    }
                    else {
                        $this->setContent('subheader', 'You don\'t have any saved recipes. Browse through the recipes A-Z or search for your favourite food then press "Add Recipe" to save.');
                    }
                    $this->setView('recipe');
                    return 1;

                } else {
                    //ask to log in
                }
                break;

            default:
                return 0;





            case 'search':
                //search methods
                if(isset($_GET['search']) && !empty($_GET['search'])) {
                    //continue
                    //will return false if possible injection occured
                    if($this->setSearchName($_GET['search'])) {
                        $this->setContent('header', 'Recipes');
                        $this->setContent('subheader', 'Browse through the recipes A-Z or search for your favourite food.');
                        $this->setAlphabettiSpaghetti();
                        //throw error if url is broken
                        if(isset($_GET['page']) && !empty($_GET['page'])) $this->setPageCount($_GET['page']);
                        else if(isset($_GET['page']) && empty($_GET['page'])) return 0;
                        else $this->setPageCount(1);
                        //setup array
                        $this->setRecipe('search');
                        $this->setPageLimits('search');
                        $this->setView('recipe');
                        if($this->hasNextPage())  $this->setDirectionalHref('search');
                    } else {
                        return 0;
                    }
                    return 1;
                }
                else if(isset($_GET['search']) && empty($_GET['search'])) return 0;
                else {}
                return 1;



            case 'default':
                $this->setContent('header', 'Recipes');
                $this->setContent('subheader', 'Browse through the recipes A-Z or search for your favourite food.');
                //default a-z
                //set up array to print a-z
                $this->setAlphabettiSpaghetti();
                //set up array of recipes
                $this->setSortLetter();
                $this->setRecipe('default');
                //save sorting letter
                //save the letter that will be called next in the pages
                $this->nextPageLetter = $this->getSortLetter();
                //throw error if url is broken
                if(isset($_GET['page']) && !empty($_GET['page'])) $this->setPageCount($_GET['page']);
                else if(isset($_GET['page']) && empty($_GET['page'])) return 0;
                else $this->setPageCount(1);

                $this->setView('recipe');
                //set up limits for the html
                $this->setPageLimits();
                if(!$this->noRecipe()) {
                    $this->setDirectionalHref('default');
                }
                return 1;
            default:
                return 0;
        }
    }

    function isSaved() {
        return $this->saved;
    }

    function setIsSaved() {
        if(Functions::isRecipeSaved($this->userObj->getId(), $this->recipeId)) $this->saved = true;
        else $this->saved = false;
    }

    function setView($view) {
        $this->view = $view;
    }

    function getView() {
        return $this->view;
    }

    function searchSet() {
        if($this->search) return 1;
        else return 0;
    }

    function setRecipeId($id) {
        $this->recipeId = $id;
    }

    function getRecipeId() {
        return $this->recipeId;
    }



    function setDirectionalHref($type) {
        switch($type) {
            case 'search':
                $this->nextPageHref = '?action=search&search=' . $this->getSearchName() . '&page=' . $this->getNextPage();
                break;
            case 'default':
                $this->nextPageHref = '?action=default&sort=' . $this->getNextPageLetter() . '&page=' . $this->getNextPage();
                break;
        }

        if(isset($_SERVER['HTTP_REFERER'])) {
            try {
                if(sizeof($this->content['alphabet']) > 0) {
                    if ($this->getSortLetter() != $this->content['alphabet'][0]) {

                        $this->previousPage = $_SERVER['HTTP_REFERER'];
                    } else if ($this->getPageCount() > 1) {
                        $this->previousPage = $_SERVER['HTTP_REFERER'];
                    } else {
                    }
                }
            } catch(RuntimeException $e) {

            }

        }
    }

    function setSearchResultDetails() {
        $this->setDefaultMessage('<div class="clear push_twentyfive"><h3 class="red inline">' .sizeof($this->recipes) .'</h3><p class="light-grey inline">&nbsp; search results for </p><h3 class="red inline">' . $this->getSearchName() . '</h3></div>');
        $search = 1;
    }

    function hasPreviousPage() {
        if($this->previousPage) return 1;
        else return 0;
    }

    function getPreviousPage() {
        return $this->previousPage;
    }


    function getNextPageHref() {
        return $this->nextPageHref;
    }





    function setAlphabettiSpaghetti($type = '') {
        $titleArray = ($this->setRecipeTitleArray($type));
        $this->setContent('alphabet', Functions::getAlphabet($titleArray));
    }

    function setError($error) {
        $this->error= $error;
    }


    function getError() {
        return $this->error;
    }

    function setDefaultMessage($message) {
            $this->defaultMessage = $message;
    }

    function getDefaultMessage() {
        return $this->defaultMessage;
    }




    function setRecipeTitleArray($type) {
        $des = array();
        switch($type) {
            case 'myrecipe':
                $recipes = Functions::getAllSavedRecipe($this->userObj->getId());
                break;
            default:
                $recipes = Functions::getAllRecipe();
                break;
        };


        foreach($recipes as $rec) {
            array_push($des, $rec->getTitle());
        }
        return $des;
    }






    function setPageLimits($type = 'default') {
        switch($type) {
            case 'default':

                $pageLimit = $this->getPageCount() * $this->numPerPage;
                $this->listStartAt = $pageLimit - $this->numPerPage;


                if ($pageLimit >= $this->getSizeOfArray($this->recipes)) {
                $this->pageLimit = $this->getSizeOfArray($this->recipes);
                if (! ($this->isEndOfRecipes())) $this->setNextPageLetter();
                } else {
                    $this->pageLimit = $pageLimit;
                    $this->setNextPageLetter($this->getSortLetter());
                }
                break;
            case 'search':
                $pageLimit = $this->getPageCount() * $this->numPerPage;
                $this->listStartAt = $pageLimit - $this->numPerPage;

                if ($pageLimit >= $this->getSizeOfArray($this->recipes)) {
                    $this->pageLimit = $this->getSizeOfArray($this->recipes);
                } else {
                    $this->pageLimit = $pageLimit;
                    $this->setNextPage($this->getPageCount()+1);
                }
                break;
            default:
                break;
    }
    }





    function setNextPageLetter($letter='') {
        if(!empty($letter)) {
            $this->nextPageLetter = $letter;
            $this->setNextPage($this->getPageCount()+1);
        }
        else {
            //set next letter and reset page to 1
            $position = array_search($this->getSortLetter(), $this->content['alphabet']);
            if(sizeof($this->content['alphabet']) > 1 && sizeof($this->content['alphabet']) != $position+1) $this->nextPageLetter = $this->content['alphabet'][$position+1];
            $this->setNextPage(1);
        }
    }

    function isEndOfRecipes() {
        if($this->noRecipe()) {
            for ($i = 0; $i < $this->getSizeOfArray($this->content['alphabet']); $i++) {
                if ($this->getSortLetter() == $this->content['alphabet'][$i]) {
                    //if last letter in the alphabet then don't set a next page href
                    if (!($i + 1 < $this->getSizeOfArray($this->content['alphabet']))) {
                        $this->nextPageLetter = 0;
                        $this->setNextPage(1);
                        $this->setHasNextPage(0);
                        return 1;
                    } else {
                        return 0;
                    }
                }
            }
        }
    }

    function setHasNextPage($boo) {
        $this->hasNextPage = $boo;
    }

    function hasNextPage() {
        return $this->hasNextPage;
    }







    function getNextPageLetter() {
        return $this->nextPageLetter;
    }





function noRecipe() {
    if(isset($this->content['alphabet']) && sizeof($this->content['alphabet']) < 1) {
        return 1;
    } else {
        return 0;
    }
}





    function getListStartAt() {
        return $this->listStartAt;
    }



    function setSearchName($name) {
        if($name = Functions::escapeInjection($name)) {
            $this->searchName = $name;
            return 1;
        } else {
            $this->setErrorMessage('Sorry it looks like our system doesn\'t like your search');
            return 0;
        }

    }

    function getSearchName() {
        return $this->searchName;
    }

    function recipeFilled() {
        if(sizeof($this->recipes) >= 1) {
            return 1;
        } else {
            return 0;
        }
    }




    function setRecipe($type) {

        $this->setContent('recipe', array());

        switch($type) {
            case 'default':
                $arr = Functions::getAllRecipe($this->getSortLetter());
                foreach($arr as $recipe) {
                    array_push($this->recipes, $recipe);
                }
                break;
            case 'search':
              foreach(Functions::getRecipeBySearchName($this->getSearchName()) as $recipe) {
                    array_push($this->recipes, $recipe);
                }
                $this->setSearchResultDetails();
                break;
            case 'singlerecipe':
                $this->recipes  = Functions::getRecipeById($this->getRecipeId());
                break;
            case 'myrecipe':
                $this->recipes  = Functions::getAllSavedRecipe($this->userObj->getId());
                break;
        }

    }












    function setSortLetter() {
        if(isset($_GET['sort']) && !empty($_GET['sort'])) {
            $this->sortLetter = $_GET['sort'];
        } else {
            if($this->noRecipe()) {

            } else {
                $this->sortLetter = $this->content['alphabet'][0];
            }

        }

    }








    function getSortLetter() {
        return $this->sortLetter;
    }







    function setPageCount($pageCount) {
        $this->pageCount = $pageCount;
    }








    function getPageCount() {
        return $this->pageCount;
    }







    public function getSizeOfArray($array) {
        return sizeof($array);
    }






    public function getPageLimit() {
        return $this->pageLimit;
    }






    public function setNextPage($nextPage) {
        $this->nextPage = $nextPage;
    }







    public function getNextPage() {
        return $this->nextPage;
    }








}
?>