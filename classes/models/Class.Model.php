<?php
    abstract class Model {

        public $db;
        public $userObj;
        public $variables = array();
        public $content = array();
        protected $key = '';
        public $userInQuestion;
        private $nav;
        private $venue = array();
        protected  $recipes = array();


        function __construct($title, $stylesheet = array(STYLESHEET, 'fullcalendar', 'jquery-ui'), $javascript = array('jquery', 'date', 'moment.min', 'jquery-ui.min', 'fullcalendar', 'gcal', 'main', ) ) {
            //main data loads in here.
            global $db;
            global $userObj;

            (LOGGED_IN ? $this->userObj = $userObj : '');
            $this->setTitle($title);
            $this->setStyleSheet($stylesheet);
            $this->setJavaScript($javascript);
            $this->db = $db;
            $this->setNav();
        }

        /**
         * set the title of the page
         */
        function setTitle($title) {
            $this->variables['title'] = $title;
        }
        /**
         * specify the name of the css file without the .css
         */
        function setStyleSheet($file) {
            if(is_array($file)) {
                for($i=0; $i<sizeof($file); $i++) {
                    $this->variables['css'][$i] = CSS.$file[$i].'.css';
                }
            } else  $this->variables['css'][0] = CSS.$file.'.css';
        }
        /**
         * specify the name of the javascript file without the .js
         */
        function setJavaScript($file) {
            for($i=0; $i<sizeof($file); $i++) {
                $this->variables['js'][$i] = JS.$file[$i].'.js';
            }
        }

        function setContent($key, $content) {
            $this->content[$key] = $content;
        }

        abstract function getContent();

        function setMessage($m) {
            $this->setContent('confirmation-message', $m);
        }

        function setNav() {
            (LOGGED_IN) ? $this->nav = HTML  . 'usernav.html' : $this->nav = HTML . 'publicnav.html';
        }

        function getNav() {
            return $this->nav;
        }



        function setStaff() {
            $this->setContent('staff', array());
            $query = "
                        SELECT
                            *
                        FROM
                            staff
                     ";
            foreach($this->db->iterate($query) as $staff) {
                array_push($this->content['staff'], array(
                    'id'        => $staff->id,
                    'name'      => $staff->name,
                    'positiion' => $staff->position
                ));
            }
        }


        function setUserInQuestion($id) {
            $this->userInQuestion = new ClientUser($id);
            if(Functions::isUser($this->userInQuestion->getId())) return $this->userInQuestion;
            else return 0;
        }



        /**
         * Function is used across multiple models therefore is inherited
         *
         * params 'thisUser' or 'userInQuestion'
         */

        function setAppointments($appointments) {

            //set appointments

            $this->setContent('appointment', array());

            /*
             * Each appointment will be accessed in the HTML like this :-
             *
             * $this->model->content['appointment'][index][id]
             * $this->model->content['appointment'][index][date]
             * ...etc
             *
             * Or iterate through them:-
             * foreach($this->model->content['appointment'] as $appoint=>$key) {
                            echo  $key['id'].' ';
                            echo  $key['date'];
                            echo '</br>';
                        }
             *
             */
            foreach($appointments as $app) {
                array_push($this->content['appointment'], array(
                        'id'        =>  $app->id,
                        'sDate'     =>  Functions::dateAsStr($app->sDate),
                        'eDate'     =>  Functions::dateAsStr($app->eDate),
                        'staff'     =>  $app->staff,
                        'notes'     =>  $app->notes,
                        'attended'  =>  $app->attended,
                        'start'     =>  $app->start,
                        'end'       =>  $app->end,
                        'user'      =>  $app->user,
                        'userfName' =>  $app->userfName,
                        'userlName' =>  $app->userlName,
                        'break'     =>  $app->break
                    )
                );

            }
            $this->setAttendedIcon('multiple');
        }



        function setDirectionButtons($user, $from, $path) {
            $prevDate = date('Y-m-01 00:00:00', strtotime('-1 month', strtotime($from)));
            $this->setContent('previousMonth', $path . $user->getId() . '&y=' . date("Y", strtotime($prevDate)) . '&m=' . date("m", strtotime($prevDate)));
            $this->setContent('prevMonthName', date("M", strtotime($prevDate)));

            $nextDate = date('Y-m-01 00:00:00', strtotime('+1 month', strtotime($from)));
            $this->setContent('nextMonth', $path . $user->getId() . '&y=' . date("Y", strtotime($nextDate)) . '&m=' . date("m", strtotime($nextDate)));
            $this->setContent('nextMonthName', date("M", strtotime($nextDate)));

            $this->setContent('currentMonthName', date("F, Y", strtotime($from)));
        }

        //this will create spaces when displaying the text into the <p> element converting from \n\n to </br></br>
        function convertStringToViewIn($htmlElement) {
            switch($htmlElement) {
                case '<p>':
                    $this->recipes->setMethod(Functions::convertToHtml($this->recipes->getMethod()));
                    break;
                case '<textarea>':
                    $this->recipes->setMethod(Functions::convertToTextArea($this->recipes->getMethod()));
                    break;
            }

        }






        function isFirstAppointment() {
            if(isset($this->userInQuestion)) {
                return $this->userInQuestion->getIsFirstAppointment();
            } else return $this->userObj->getIsFirstAppointment();
        }

        function setVenue() {
            //create array of venues
            $query = "
                        SELECT
                            *
                        FROM
                          venue
                        ORDER BY
                          id
                     ";
            foreach($this->db->iterate($query) as $res) {
                array_push($this->venue, new Venue(
                    $res->id,
                    $res->buildingName,
                    $res->firstLineOfAddress,
                    $res->town,
                    $res->county,
                    $res->postcode
                ));
            }
        }

        function getVenue() {
            return $this->venue;
        }













        /**
         * @param $i
         *
         * Function is used across multiple models therefore is inherited
         */

        function setSingleAppointment($i) {

            if(isset($this->userInQuestion)) $u = $this->userInQuestion;
            else $u = $this->userObj;

            //get appointment from the user appointments
            $appointment = $u->getAppointmentsById($i);

            $this->setContent('appointment', array(
                'id'        =>                       $appointment->id,
                'sDate'     =>  Functions::dateAsStr($appointment->sDate),
                'eDate'     =>  Functions::dateAsStr($appointment->eDate),
                'staff'     =>                       $appointment->staff,
                'notes'     =>                       $appointment->notes,
                'attended'  =>                       $appointment->attended,
                'start'     =>                       $appointment->start,
                'end'       =>                       $appointment->end,
                'user'      =>                       $appointment->user,
                'userfName' =>                       $appointment->userfName,
                'userlName' =>                       $appointment->userlName,
                'venueBuildingName' =>               $appointment->venue->getBuildingName(),
                'venueFirstLineOfAddress'    =>               $appointment->venue->getFirstLine(),
                'venueTown'         =>               $appointment->venue->getTown(),
                'venueCounty'       =>               $appointment->venue->getCounty(),
                'venuePostcode'     =>               $appointment->venue->getPostcode(),
                'attendanceIcon' =>                  '',
                'attendanceIconDesc' => ''
            ));
            $this->setAttendedIcon('single');

        }




        /**
         * @param $i
         *
         * Function is used across multiple models therefore is inherited
         */

        function setAttendedIcon($amount) {
            switch($amount) {
                case 'multiple':
                    foreach($this->content['appointment'] as &$app) {
                        switch($app['attended']) {
                            case "-1":
                                $app['attendanceIcon'] = '../image/icon/scheduled.png';
                                $app['attendanceIconDesc'] = 'Scheduled';
                                break;
                            case "0":
                                $app['attendanceIcon'] = '../image/icon/cross.png';
                                $app['attendanceIconDesc'] = 'Missed';
                                break;
                            case "1":
                                $app['attendanceIcon'] = '../image/icon/tick.png';
                                $app['attendanceIconDesc'] = 'Attended';
                                break;
                        }
                    }
                    break;
                case 'single':
                    switch($this->content['appointment']['attended']) {
                        case '-1':
                            $this->content['appointment']['attendanceIcon'] = '../image/icon/scheduled.png';
                            $this->content['appointment']['attendanceIconDesc'] = 'Scheduled';
                            break;
                        case '0':
                            $this->content['appointment']['attendanceIcon'] = '../image/icon/tick.png';
                            $this->content['appointment']['attendanceIconDesc'] = 'Missed';
                            break;
                        case '1':
                            $this->content['appointment']['attendanceIcon'] = '../image/icon/cross.png';
                            $this->content['appointment']['attendanceIconDesc'] = 'Attended';
                            break;
                    }
                    break;
            }

        }

        function getRecipe() {
            return $this->recipes;
        }






    }
?>