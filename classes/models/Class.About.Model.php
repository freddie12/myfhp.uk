<?php

    class AboutModel extends Model {

        function getContent() {
            if(isset($_GET['id'])) $this->setContent('body', $_GET['id']);
            else $this->defaultContent();
        }

        function defaultContent() {
            //gather data from the model accoring to the request query
            $query = "SELECT
                        i
                      FROM
                        about;
                     ";

            $res = $this->db->fetchOne($query);

            $this->setContent('body', $res);
        }

    }
?>