<?php

abstract class Controller {

    protected $model;
    protected $view;
    protected $userObj;
    protected $default = true;

    public function __construct(Model $model, View $view) {
        global $userObj;
        $this->userObj  = $userObj;
        $this->model    = $model;
        $this->view     = $view;
    }

    abstract function init();
}

?>