<?php

class RecipeController extends Controller {

    function init() {
        if(isset($_GET['action']) && !empty($_GET['action'])) {
            if($this->model->init() && !$this->model->getError()) {
                if($this->model->noRecipe()) $this->model->setContent('subheader', 'No recipes avilable.');
                $this->view->setBody($this->model->getView('view'));
                $this->view->setLayout('main');
            }
            else Functions::badUrl($this->view, $this->model);
        } else {
            Functions::badUrl($this->view, $this->model);
        }
    }
}

?>