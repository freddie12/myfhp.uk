<?php

class AdminController extends Controller {

    protected $action;

    function init() {

        $this->view->setLayout('admin');
        $this->view->setBody('home');

        if(isset($_GET['action'])) {
            $this->action = $_GET['action'];
            switch($this->action) {



                case 'users':
                    $this->model->setContent('header', 'Users');
                    $this->model->setUserArray(Functions::listUsers('lName'));
                    $this->view->setBody('users');
                    break;





                case 'singleuser':
                    if(isset($_GET['i']) && !empty($_GET['i'])) {
                        $viewFrom = '';
                        $viewTo   = '';

                        if(isset($_GET['m']) && !empty($_GET['m']) && isset($_GET['y']) && !empty($_GET['y'])) {
                            $viewFrom   = $_GET['y'] . '-' . $_GET['m'] . '-01 00:00:00';
                            $viewTo     = date("Y-m-01 00:00:00", strtotime('+1 month', strtotime($viewFrom)));

                        } else {
                            $viewFrom   = date("Y-m-01 00:00:00", strtotime("now"));
                            $viewTo     = date("Y-m-01 00:00:00", strtotime('+1 month', strtotime($viewFrom)));
                        }


                        //model setUsers only works if it is passed an array of users.
                        $user = array();
                        $id = $_GET['i'];

                        array_push($user, $this->model->setUserInQuestion($id));
                        $this->model->setDirectionButtons($user[0], $viewFrom, '?action=singleuser&i=');

                        if($user[0]) {
                            $this->model->setcontent('header', $user[0]->getFullName());
                            $this->model->setUserArray($user);
                            $this->model->isFirstAppointment();
                            $this->model->setVenue();
                            $this->model->setStaff();
                            $this->model->setAppointments(Functions::getAppointmentsByMonth($user[0], $viewFrom, $viewTo));
                            $this->view->setBody('singleuser');
                            break;
                        } else {
                            $this->error($this->action, 'Something has gone wrong!');
                            break;
                        }
                    } else {break;}





                case 'appointments':
                    if(isset($_GET['date']) && !empty($_GET['date'])) {
                        //get all appointments for date
                        //set header
                        //setBody admin.appointment.date.html
                        $this->model->setAppointmentByDate($_GET['date']);
                        $this->model->setAppointments($this->model->getAppointment());
                        //appointments to show
                        $this->model->setDate($_GET['date']);
                        $this->view->setBody('adminAppointments');
                    } else {
                        $this->model->setContent('header', 'Appointments');
                        $this->view->setBody('appointments');
                    }
                    break;

                case 'addNonUser':
                    $this->model->setVenue();
                    $this->model->setStaff();
                    $this->view->setBody('nonUserForm');
                    break;




                case 'singleAppointment':
                    if(isset($_GET['u']) && !empty($_GET['u']) && isset($_GET['i']) && !empty($_GET['i'])) {
                        $user        =  array();
                        $appointment =  $_GET['i'];
                        array_push($user, $this->model->setUserInQuestion($_GET['u']));
                        if($user[0]) {
                            $this->model->setSingleAppointment($appointment);
                            $this->model->setContent('header', $this->model->content['appointment']['userfName'] . ' ' . $this->model->content['appointment']['userlName'] );
                            $this->model->setContent('subheader', $this->model->content['appointment']['sDate']);
                            $this->view ->setBody('adminSingleAppointment');
                            break;
                        } else {break;}
                    }



                case 'recipes':
                    if(isset($_GET['i']) && !empty($_GET['i'])) {
                        $this->model->setContent('Header', 'Single Recipe');
                        $this->model->setSingleRecipe(Functions::escapeInjection($_GET['i']));
                        $this->model->convertStringToViewIn('<p>');
                        $this->view->setBody('adminsinglerecipe');
                        break;
                    } else {
                        $this->model->setContent('header', 'Recipes');
                        $this->model->setAllRecipes();
                        $this->view->setBody('recipes');
                        break;
                    }

                case 'editRecipes':
                    if(isset($_GET['i']) && !empty($_GET['i'])) {
                        $this->model->setSingleRecipe(Functions::escapeInjection($_GET['i']));
                        $this->model->setEditRecipe();
                        $this->model->convertStringToViewIn('<textarea>');
                        $this->model->setContent('header', 'Edit Recipe');
                        $this->view->setBody('editRecipe');
                        break;
                    } else {
                        $this->error('error', 'Looks like you have a bad URL');
                        break;
                    }



                case 'addrecipe':
                    $this->model->setContent('header', 'Add New Recipe');
                    $this->view->setBody('addrecipe');
                    break;
            }
        }
    }

    function error($body, $message) {
        $this->model->setContent('error', $message);
        $this->view->setBody($body);
    }
}

?>