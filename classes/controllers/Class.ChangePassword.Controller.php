<?php

class ChangePasswordController extends Controller {

    /**
     *
     */
    function init() {
        $this->model->init();
        if(isset($_GET['action']) && !empty($_GET['action'])) {
            $action = ($_GET['action']);
            switch($action) {
                case 'changepassword':
                    if($result = $this->model->emailLinkPressed()) {
                        if($result == 'badurl') {
                            $this->throwError();
                            return;
                        }
                        $this->view->setLayout('main');
                        $this->view->setBody('confirmation');
                    } else {
                        $this->view->setBody('resetpassword');
                        $this->view->setLayout('main');
                    }
                    break;
            }
        }
        else {
            //default page
            $this->defaul();
        }
    }

    private function defaul() {
        if(isset($_POST['submit'])) {
            if(isset($_POST['email']) && !empty($_POST['email'])) {
                //Check if email is a belongs to a user
                if(Functions::isEmail(Functions::escapeInjection($_POST['email']))) {
                    //Continue
                    if(Functions::liveAccount($_POST['email'])){
                        $this->model->sendReset($_POST['email']);
                        $this->model->setContent('confirmation-header', 'Thank you!');
                        $this->model->setContent('confirmation-message', 'Please check your email and follow the instructions.');
                        $this->view->setLayout('main');
                        $this->view->setBody('confirmation');
                    }else {
                        $this->model->setContent('email-warning', 'please validate your email');
                        $this->defLayout();
                    }
                    
                } else {
                    //display unknown user error
                    $this->model->setContent('email-warning', 'unknown user');
                    $this->defLayout();
                }
            } else {
                //display error
                $this->model->setContent('email-warning', 'please enter an email');
                $this->defLayout();
            }
            
        } else {
            //set layout
            $this->defLayout();
        }
        
        $this->view->setLayout('main');
    }
    
    function defLayout(){
        $this->model->setContent('header', 'Forgotten Password');
        $this->model->setContent('subheader', 'Please enter your email address.');
        $this->view->setBody('enteremail');
    }

    function throwError() {
        Functions::badUrl($this->view, $this->model);
    }



    function setContent() {

    }

    function newFunctionMade() {
        
    }
}

?>