<?php

class AccountController extends Controller {

    private $user;
    private $viewFrom;
    private $viewTo;
    private $badUrl;


    function init() {
        global $userObj;
        $this->user = $userObj;
        $this->model->init();
        $this->badUrl = Functions::badUrl($this->view, $this->model);


        if(isset($_GET['m']) && !empty($_GET['m']) && isset($_GET['y']) && !empty($_GET['y'])) {
            $this->viewFrom   = $_GET['y'] . '-' . $_GET['m'] . '-01 00:00:00';
            $this->viewTo     = date("Y-m-01 00:00:00", strtotime('+1 month', strtotime($this->viewFrom)));

        } else {
            $this->viewFrom   = date("Y-m-01 00:00:00", strtotime("now"));
            $this->viewTo     = date("Y-m-01 00:00:00", strtotime('+1 month', strtotime($this->viewFrom)));
        }

        if(isset($_GET['action']) && !empty($_GET['action'])) {
            $action = ($_GET['action']);
            switch($action) {
                case 'viewAllAppointments':
                    $this->viewAllAppointments();
                    $this->model->setStaff();
                    $this->model->variables['title'] = "Appointments";
                    break;
                case 'singleAppointment':
                    if(isset($_GET['i']) && !empty($_GET['i'])) {
                            $this->viewSingleAppointment($_GET['i']);
                            break;
                    } else {
                        $this->badUrl;
                        break;
                    }
                case 'changepassword':
                    $this->changePassword();
                    break;
                case 'confirmAppointment':
                    $this->confirmAppointment();
                    break;
                case 'cancelAppointmentRequest':
                    $this->cancelAppointment();
                    break;
                case 'editname':
                    $this->editname();
                    break;
                case 'editemail':
                    $this->editemail();
                    break;
                case 'changephone':
                    $this->changephone();
                    break;
            }
        }
        else {
            $this->model->setNextAppointmentIndex();
            $this->model->setContent('nextAppointmentMessage', $this->model->createNextAppointmentMessage($this->model->content['nextAppointmentIndex']));
            $this->model->setAppointments(Functions::getAppointmentsByMonth($this->user, $this->viewFrom, $this->viewTo));//sets the content for the html to pick up
            $this->model->setUserDetails();
            $this->view->setBody('account');
            $this->view->setLayout('main');
        }
    }

    private function confirmAppointment() {
        if(isset($_GET['key']) && !empty($_GET['key'])) {
            $key = $_GET['key'];
            //check key links to an appointment
            if(Functions::checkKey($key, 'appointment')) {
                if($this->model->confirmAppointment($key)) {
                    $this->model->setContent('confirmation-header', 'Appointment Confirmed');
                    $this->model->setContent('confirmation-message', 'An email has been sent to the client.');
                    $this->view->setBody('confirmation');
                    $this->view->setLayout('main');
                } else {
                    echo 'Database connection failure: please contact Alex with message(Appointment deletion failure, keyref = ' . $key .')';
                }
            } else {
                //key doesn't exist
                $this->badUrl;
            }
        } else {
            //no key given in url
            $this->badUrl;
        }
    }

    private function cancelAppointment() {
        if(isset($_GET['key']) && !empty($_GET['key'])) {
            $key = $_GET['key'];
                //check key links to an appointment
            if(Functions::checkKey($key, 'appointment')) {
                if($this->model->deleteAppointment($key)) {
                $this->model->setContent('confirmation-header', 'Appointment Cancelled');
                $this->model->setContent('confirmation-message', 'Please notify your client.');
                $this->view->setBody('confirmation');
                $this->view->setLayout('main');
            } else {
                echo 'Database connection failure: please contact Alex with message(Appointment deletion failure, keyref = ' . $key .')';
            }
            } else {
                //key doesn't exist
                $this->badUrl;
            }
            } else {
                //no key given in url
                $this->badUrl;
        }
}

    private function viewAllAppointments() {
        $this->model->setDirectionButtons($this->user, $this->viewFrom, '?action=viewAllAppointments&i=');
        $this->model->setAppointments(Functions::getAppointmentsByMonth($this->user, $this->viewFrom, $this->viewTo));
        $this->model->setVenue();
        $this->model->setContent('header', $this->model->userObj->getFullName());
        $this->model->setContent('subheader', '');
        $this->view->setBody('accountAppointments');
        $this->view->setLayout('main');
    }

    private function viewSingleAppointment($i) {
        $this->model->setSingleAppointment($i);
        $this->model->setContent('header', 'Appointment');
        $this->model->setContent('subheader', 'Appointment');
        $this->view->setBody('singleAppointment');
        $this->view->setLayout('main');
    }

    private function changePassword() {
        if(isset($_POST['submit'])) {
            if($this->model->passwordChangeSubmit()) {
                $this->view->setBody('confirmation');
                $this->view->setLayout('main');
                return 1;
            }
        }
        //display warnings
        $this->model->setContent('header', 'Change Password');
        $this->model->setContent('subheader', '');
        $this->view->setBody('changepassword');
        $this->view->setLayout('main');
    }

    private function changePhone() {
        //display warnings
        $this->model->setContent('header', 'Change Phone Number');
        $this->model->setContent('subheader', '');
        $this->view->setBody('changephone');
        $this->view->setLayout('main');
    }

    function editname() {
        $this->model->setContent('header', 'Edit Details');
        $this->view->setBody('editname');
        $this->view->setLayout('main');
    }

    function editemail() {
        $this->model->setContent('header', 'Change Email');
        $this->model->setContent('subheader', 'Once you submit your new email you will be required to validate it. Please go to your email account and follow the instructions. Only then will your new email become available.');
        $this->view->setBody('editemail');
        $this->view->setLayout('main');
    }

    function setContent() {

    }

    function newFunctionMade() {
        
    }
}

?>