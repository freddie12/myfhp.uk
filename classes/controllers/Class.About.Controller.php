<?php

class AboutController extends Controller {

    function __construct(AboutModel $model) {
        $this->model = $model;
    }

    function init() {
        $this->model->getContent();
    }

}

?>