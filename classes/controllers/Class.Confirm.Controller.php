<?php

class ConfirmController extends Controller {

    function init() {
        if(isset($_GET['action']) && !empty($_GET['action'])) {
            if($this->model->init() && !$this->model->getError()) {
                $this->model->setContent('confirmation-header', 'Email changed');
                $this->model->setContent('confirmation-message', 'You have successfully changed your email address.');
                $this->view->setBody('confirmation');
                $this->view->setLayout('main');
            }
            else Functions::badUrl($this->view, $this->model);
        } else {
            Functions::badUrl($this->view, $this->model);
        }
    }
}

?>