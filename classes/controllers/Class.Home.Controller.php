<?php

class HomeController extends Controller {

    function __construct(HomeModel $model) {
        $this->model = $model;
    }

    function init() {
        $this->model->getContent();
    }

}