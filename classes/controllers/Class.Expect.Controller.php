<?php

class ExpectController extends Controller {

    function __construct(ExpectModel $model) {
        $this->model = $model;
    }

    function init() {
        $this->model->getContent();
    }

}

?>