<?php

class RegisterController extends Controller {

    function init() {
        //inititalise variables for html
        $this->model->details['checkbox'] = '0';
        $this->model->resetWarning();

        //user submits form
        if(isset($_POST['submit'])) {
            if($this->model->validateForm()) {
                //form filled correctly
                $this->model->register();
                $this->view->setBody('confirmation');
                $this->view->setLayout('main');
                $this->default = false;

            }
        }

        //user has clicked on validate email link with KEY and USER_ID
        if(isset($_GET['i']) && !empty($_GET['i']) && isset($_GET['u']) && !empty($_GET['u'])) {
            //get details of user
            if($this->model->checkUserKey($_GET['i'], $_GET['u'])) {
                $this->model->validateUser();
            }
            $this->view->setBody('confirmation');
            $this->view->setLayout('main');
            $this->default = false;
        }


        if($this->default) {
                $this->view->setBody('register');
                $this->view->setLayout('main');
        }
    }

}

?>