<?php

class _404Controller extends Controller {

    function init() {
        $this->view->setBody('404');
        $this->view->setLayout('main');
    }
}

?>