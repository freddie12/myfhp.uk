<?php

Class Recipe {

    private $id;
    private $title;
    private $description;
    private $method;
    private $ingredients  = array();

    function __construct($id, $t, $d, $m, $i) {
        $this->id           = $id;
        $this->title        = $t;
        $this->description  = $d;
        $this->method       = $m;
        $this->ingredients  = explode('|', $i);
    }

    public function getId() {
        return $this->id;
    }

    public function getTitle() {
        return $this->title;
    }

    public function getDescription() {
        return $this->description;
    }

    public function getMethod() {
        return $this->method;
    }

    public function getIngredients() {
        return $this->ingredients;
    }

    public function setIngredients($i) {
        $this->ingredients = $i;
    }

    public function setMethod($method) {
        $this->method = $method;
    }
}

?>
