<?php

abstract class View {

    public  $model;
    private $body;
    private $layout;

    function __construct($model) {
        $this->model = $model;
    }


    /**
     * specify what you want inside the layout
     */
    function setBody($body) {
        $this->body = HTML.$body.'.html';
    }
    function getBody() {
        require_once($this->body);
    }


    /**
     * specify the layout you want (the layout is the wrapper around the main body e.g. header->body->footer)
     */
    function setLayout($layoutName) {
        $this->layout = LAYOUT.$layoutName.'.html';
    }

    function getLayout() {
        return $this->layout;
    }


    abstract function output();


}

?>