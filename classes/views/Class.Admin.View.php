<?php
class AdminView extends View {

    function output() {
        if(!LOGGED_IN) $this->setLayout('login');
        else if(!ADMIN) {
            header('Location: ../home');
        }

        require_once($this->getLayout());
    }

}
?>