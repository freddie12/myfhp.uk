<?php

    class AboutView extends View {

        function output() {
            $this->setBody('about');
            $this->setLayout('main');
            require_once($this->getLayout());
        }

    }

?>