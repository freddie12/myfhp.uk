<?php

    class HomeView extends View {

        function output() {
            $this->setBody('home');
            $this->setLayout('main');
            require_once($this->getLayout());
        }

    }

?>