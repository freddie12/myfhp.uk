<?php

class RecipeView extends View {

    function output() {
        if(!LOGGED_IN) $this->setLayout('login');
        require_once($this->getLayout());
    }
}

?>