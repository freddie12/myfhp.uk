<?php

abstract class User {

    protected   $id;
    private     $dob;
    protected   $fName;
    protected   $lName;
    protected   $details = array();
    protected   $appointment = array();
    protected   $db;
    protected   $newsletter;
    protected   $phone;

    protected function __construct($id) {
        global $db;
        $this->db = $db;
        $this->setId($id);
        $query = "SELECT
                *
              FROM
                client_user
              WHERE
                  id = " . $this->id . ";
             ";
        $this->details = $db->fetchOneRow($query);
        $this->init();
    }

    private function init() {
        $this->setfName();
        $this->setlName();
        $this->setDob();
        $this->setNewsletter();
        $this->setAdmin();
        $this->setPhone();
    }

    private function setPhone() {
        $this->phone = $this->details->phone;
    }

    public function getPhone() {
        return $this->phone;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getId() {
        return $this->id;
    }

    public function setDob() {
        $this->dob = $this->details->dob;
    }

    public function getDob() {
        return $this->dob;
    }

    protected function setfName() {
        $this->fName = $this->details->fName;
    }

    protected function setlName() {
        $this->lName = $this->details->lName;
    }

    public function getfName() {
        return $this->fName;
    }

    public function getlName() {
        return $this->lName;
    }

    public function getFullName() {
        return $this->getfName() . ' ' . $this->getlName();
    }


    protected function setNewsletter() {
        $this->newsletter = $this->details->newsletter;
    }

    public function getNewsletter() {
        return $this->newsletter;
    }

    public function setAdmin() {
        $this->admin = $this->details->admin;
    }

    public function getAdmin() {
        return $this->admin;
    }

}


?>
