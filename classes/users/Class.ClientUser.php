<?php

class ClientUser extends User {


    private $verified;
    private $key;
    private $email;
    protected $appointment = array();
    protected  $admin;
    private  $recipe = array();
    private $condition = array();
    private $firstAppointment = true;//unless changed later.


    public function __construct($id) {
        parent::__construct($id);
        $this->init();
    }

    private function init() {
        $this->setEmail();
        $this->setDob();
        $this->setKey();
        $this->setVerified();
        $this->setAppointmentList();
        $this->setRecipe();
        $this->setIsFirstAppointment();
    }

    private function setIsFirstAppointment() {
        global $db;

        $query = "SELECT
                      count(id) as quantity
                 FROM
                    appointment
                  WHERE
                    client_user = '".$this->getId()."'
                  AND
                    confirmed = '1';
                 ";
        $res = $db->fetchOneRow($query);

        for($i=0;$i<4;$i++) {

        }

        if($res->quantity > 0) $this->firstAppointment = 0;
        else $this->firstAppointment     = 1;
    }

    public function getIsFirstAppointment() {
        return $this->firstAppointment;
    }

    private function setEmail() {
        $this->email = $this->details->email;
    }

    public function getEmail() {
        return $this->email;
    }

    public function getName() {
        return parent::getfName();
    }

    public function getlName() {
        return parent::getlName();
    }

    public function getFullName() {
        return parent::getFullName();
    }

    private function setKey() {
        $this->key = $this->details->validation_key;
    }

    public function getKey() {
        return $this->$key;
    }

    private function setVerified() {
        $this->verified = $this->details->verified;
    }

    public function getVerified() {
        return $this->verified;
    }


    public function dumpUser() {
        $dump = array
                    (
                        'id' => $this->getId(),
                        'fName' => $this->getfName(),
                        'lName' => $this->getlName(),
                        'email' => $this->getEmail(),
                        'dob' => $this->getDob(),
                        'verified' => $this->getVerified(),
                        'admin' => ADMIN

                    );
        var_dump($dump);
    }


    public function setAppointment(Appointment $appointment) {
        array_push($this->appointment, $appointment);
    }

    public function getAppointments() {
        return $this->appointment;
    }

    public function getRecipe() {
        return $this->recipe;
    }

    private function setRecipe() {
        $query = "  SELECT
                         r.id, r.title, r.description, r.ingredients, r.method
                    FROM
                        client_user cu, user_recipe ur, recipe r
                    WHERE
                        cu.id = ur.client_user
                    AND
                        r.id = ur.recipe
                    AND
                        cu.id = '" . $this->getId() . "'
                 ";

        foreach($this->db->iterate($query) as $recipe) {
            array_push($this->recipe, new Recipe(
                $recipe->id,
                $recipe->title,
                $recipe->description,
                $recipe->method,
                $recipe->ingredients
            ));
        };
    }






    function setAppointmentList($byId = ''){

        $this->appointment = array();

        $query = "SELECT
                        a.id as 'id',
                        a.start,
                        a.end,
                        a.notes,
                        a.attended,
                        a.confirmed,
                        s.name as 'staffName',
                        cu.fName as 'userfName',
                        cu.lName as 'userlName',
                        cu.id as 'client_id',
                        v.id as venue_id
                    FROM
                        appointment a,
                        client_user cu,
                        staff s,
                        venue v
                    WHERE
                        cu.id = a.client_user
                        AND
                        s.id = a.staff
                        AND
                        client_user = '" .$this->getId(). "'
                        AND
                        v.id = a.venue
                    ORDER BY
                        start DESC";

        if(!empty($byId)) $query .= "AND
                                a.id = " . $byId;

        foreach ($this->db->iterate($query) as $row) {
            $app = new Appointment
            (
                $row->id,
                $row->start,
                $row->end,
                $row->client_id,
                $row->userfName,
                $row->userlName,
                $row->staffName,
                $row->notes,
                $row->attended,
                $row->venue_id,
                '',
                $row->confirmed
            );
            $this->setAppointment($app);
        }
    }

    function getAppointmentsById($i) {
        foreach($this->getAppointments() as $app=>$key) {
            if($key->id == $i) return $key;
        }
    }

    function getNextAppointmentIndex() {
        $appArr = array();

        //store all timestamps
        foreach($this->getAppointments() as $app) {
            array_push($appArr, $app->sDate);
        }

        //get index of next upcoming date
        $nextDateIndex = Functions::nextDate($appArr);
        return $nextDateIndex;
    }

    function createAppointmentMessage($index) {
        $appointment = $this->getAppointments();
        $appointment = $appointment[$index];
        //set spec mess
        $message = "Your next appointment is on </br>";
        $message .= Functions::dateAsStr($appointment->sDate);
        $message .= "</br> from </br>";
        $message .= $appointment->start . '&nbsp;&nbsp;to&nbsp;&nbsp;' . $appointment->end;
        $message .= "</br> with </br>";
        $message .= $appointment->staff;

        return $message;
    }

    function getDefaultAppointmentMessage() {
        return "You have no scheduled appointments</br>";
    }
}
?>