<?php
require_once('../config/config.php');
require_once(CONTROLLERS    . 'Class.Account.Controller.php');
require_once(MODELS         . 'Class.Account.Model.php');
require_once(VIEWS          . 'Class.ChangePassword.View.php');

$model       = new AccountModel('Account');
$view        = new ChangePasswordView($model);
$controller  = new AccountController($model, $view);

$controller->init();
$view->output();
?>