# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.6.21)
# Database: fhp
# Generation Time: 2015-01-31 13:17:13 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table appointment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `appointment`;

CREATE TABLE `appointment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `client_user` int(11) NOT NULL,
  `notes` varchar(10000) NOT NULL DEFAULT 'No notes have been made for this appointment.',
  `staff` int(11) NOT NULL,
  `attended` tinyint(1) NOT NULL DEFAULT '-1',
  `validation_key` varchar(20) DEFAULT NULL,
  `venue` int(255) DEFAULT NULL,
  `confirmed` int(1) DEFAULT '0',
  `break` tinyint(1) NOT NULL DEFAULT '0',
  `reminder_sent` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `client_user` (`client_user`),
  KEY `staff` (`staff`),
  KEY `appointment_ibfk_3` (`venue`),
  CONSTRAINT `appointment_ibfk_1` FOREIGN KEY (`client_user`) REFERENCES `client_user` (`id`) ON DELETE CASCADE,
  CONSTRAINT `appointment_ibfk_2` FOREIGN KEY (`staff`) REFERENCES `staff` (`id`) ON DELETE CASCADE,
  CONSTRAINT `appointment_ibfk_3` FOREIGN KEY (`venue`) REFERENCES `venue` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `appointment` WRITE;
/*!40000 ALTER TABLE `appointment` DISABLE KEYS */;

INSERT INTO `appointment` (`id`, `start`, `end`, `client_user`, `notes`, `staff`, `attended`, `validation_key`, `venue`, `confirmed`, `break`, `reminder_sent`)
VALUES
	(251,'2015-01-30 15:00:00','2015-01-30 16:29:00',2,'No notes have been made for this appointment.',1,-1,'xjxsr0fwD2GP2rO8RnuI',1,1,0,0),
	(252,'2015-01-30 16:30:00','2015-01-30 17:59:00',13,'No notes have been made for this appointment.',1,-1,NULL,1,1,0,0),
	(253,'2015-02-19 09:30:00','2015-02-19 10:59:00',3,'No notes have been made for this appointment.',1,-1,NULL,1,1,0,0);

/*!40000 ALTER TABLE `appointment` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
