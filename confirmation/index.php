<?php
require_once('../config/config.php');
require_once(CONTROLLERS    . 'Class.Confirm.Controller.php');
require_once(MODELS         . 'Class.Confirm.Model.php');
require_once(VIEWS          . 'Class.Confirm.View.php');

$model       = new ConfirmModel('Confirm');
$view        = new ConfirmView($model);
$controller  = new ConfirmController($model, $view);

$controller->init();
$view->output();
?>