<?php
    session_start();
    //destroy cookie
    if(isset($_COOKIE['session_user_id'])) {
        setcookie("session_user_id", "", time() - 3600, "/");
    }

    //destroy session
    unset($_SESSION['session_user_id']);
    session_destroy();
    //move page
    header('Location: ../home/');
?>