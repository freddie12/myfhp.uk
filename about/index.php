<?php

require_once('../config/config.php');

require_once(CONTROLLERS . 'Class.About.Controller.php');
require_once(MODELS . 'Class.About.Model.php');
require_once(VIEWS . 'Class.About.View.php');

$model = new AboutModel('About');
$view = new AboutView($model);
$controller = new AboutController($model, $view);
//echo $_SERVER['REQUEST_URI'];
$controller->init();
$view->output();

?>