<?php

//make sure you spell view correctly!
require_once('../config/config.php');
require_once(CONTROLLERS . 'Class.Home.Controller.php');
require_once(MODELS . 'Class.Home.Model.php');
require_once(VIEWS . 'Class.Home.View.php');


$model = new HomeModel('Home');
$view = new HomeView($model);
$controller = new HomeController($model, $view);

$controller->init();

$view->output();

?>


