<?php

require_once('../config/config.php');
$number = trim($_POST['phone']);
$id     = Functions::escapeInjection(trim($_POST['id']));

if(isset($number) && !empty($number)) {
    if (!Functions::escapeInjection($number) || !is_numeric($number)) {
        echo 0;
    }
}
if(Functions::updatePhone($number, $id)) {
    echo 1;
}
else {
    echo 2;
}
?>
