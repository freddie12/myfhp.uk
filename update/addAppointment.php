<?php

    require_once('../config/config.php');
    global $sendMail;

    if(strtotime($_POST['s']) < strtotime("now")) {
        //date in past error
        echo 2;//return to ajax
    } else if(Functions::dateClash($_POST['s'], $_POST['e'])) {
        //dateclashed error
        echo 0;//return to ajax
    } else {
            if(Functions::escapeInjection($_POST['s']) &&
                Functions::escapeInjection($_POST['e']) &&
                    Functions::escapeInjection($_POST['u']) &&
                        Functions::escapeInjection($_POST['v']) &&
                            Functions::escapeInjection($_POST['auth'])
            ) {

                if($_POST['auth'] == 'user') {

                    for($i=0;$i<2;$i++){}
                    $key = Functions::createRandomKey('appointment');

                    //get user details
                    $user = new ClientUser($_POST['u']);
                    $emailUserEmail                             = $user->getEmail();
                    $emailUserName                              = $user->getFullName();
                    $emailTimeFrom                              = date("H:i", strtotime($_POST['s']));
                    $emailTimeTo                                = date("H:i", (strtotime($_POST['e'])+60));
                    $emailDate                                  = Functions::dateAsStr($_POST['s']);

                    //get venue details
                    $venue = Functions::getVenue($_POST['v']);
                    $emailAddressBuildingName                   = $venue->getBuildingName();
                    $emailAddressFirstLineOfAddress             = $venue->getFirstLine();
                    $emailAddressTown                           = $venue->getTown();
                    $emailAddressCounty                         = $venue->getCounty();
                    $emailAddressPostcode                       = $venue->getPostcode();

                    //staff details
                    $staff = Functions::getStaff($_POST['st']);
                    $emailStaffName     =   $staff->name;
                    $emailStaffPhone    =   $staff->phone;
                    $emailStaffEmail    =   $staff->email;

                    //links
                    $emailLinkA = THIS_DOMAIN . 'ammendBooking/?key=' . $key . '&action=confirmAppointment';
                    $emailLinkB = THIS_DOMAIN . 'ammendBooking/?key=' . $key . '&action=cancelAppointmentRequest';
                    require_once(HTML . 'email.appointment.request.html');

                    //email staff member
                    if($sendMail->carrierPigeon($emailStaffEmail, 'Appointment', $content, 'noreply')) {
                        //update database
                        $query = "  INSERT INTO
                                      appointment(start, end, client_user, staff, validation_key, venue)
                                    VALUES
                                    (
                                            '" . $_POST['s']  . "',
                                            '" . $_POST['e']  . "',
                                            '" . $_POST['u']  . "',
                                            '" . $_POST['st'] . "',
                                            '" . $key         . "',
                                            '" . $_POST['v']  . "'
                                        )
                                    ";


                      if($db->quickQuery($query)) echo 1;
                      else echo 0;
                    } else {
                        echo 0;
                    }
                } else {
                /**
                 * Made by Admin : email the user confirmation
                 */
                 //user details
                 $user = new ClientUser($_POST['u']);
                 $emailUserName = $user->getFullName();
                $userEmail = $user->getEmail();

                //time
                $emailTimeFrom = date("H:i", strtotime($_POST['s']));
                $emailTimeTo   = date("H:i", (strtotime($_POST['e'])+60));
                $emailDate = Functions::dateAsStr($_POST['s']);

                //get venue details
                $venue = Functions::getVenue($_POST['v']);
                $emailAddressBuildingName                   = $venue->getBuildingName();
                $emailAddressFirstLineOfAddress             = $venue->getFirstLine();
                $emailAddressTown                           = $venue->getTown();
                $emailAddressCounty                         = $venue->getCounty();
                $emailAddressPostcode                       = $venue->getPostcode();

                //staff details
                $staff = Functions::getStaff($_POST['st']);
                $emailStaffName     =   $staff->name;
                $emailStaffPhone    =  $staff->phone;
                $emailStaffEmail    = $staff->email;
                $emailStaffEmailLink = 'mailto:' . $emailStaffEmail;

                require_once(HTML . 'email.appointment.html');
                if($sendMail->carrierPigeon($userEmail, 'Appointment', $content, 'noreply')) {
                    //enter into database
                    $query = "
                        INSERT INTO
                          appointment(start, end, client_user, staff, venue, confirmed)
                        VALUES
                            (
                                '" . $_POST['s']  . "',
                                '" . $_POST['e']  . "',
                                '" . $_POST['u']  . "',
                                '" . $_POST['st'] . "',
                                '" . $_POST['v']  . "',
                                '1'
                            )
                        ";
                    if($db->quickQuery($query)) echo 1;
                    else echo 0;
                } else {
                    echo 0;
                }
        }
    } else {
        echo 0;
    }
}
?>
