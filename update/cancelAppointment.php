<?php

    require_once('../config/config.php');
                global $db;
                if(isset($_POST['id']) && !empty($_POST['id']) && isset($_POST['auth']) && !empty($_POST['auth'])) {
//                    debug
//                    for($i=0;$i<2;$i++){}

                    //clean string
                    if(Functions::escapeInjection($_POST['id']) && $auth = Functions::escapeInjection($_POST['auth'])) {
                        $id     = $_POST['id'];
                        $auth   = $_POST['auth'];
                        if($res = Functions::getAppointmentById($id)) {
                            //get user details
                            for($i=0;$i<2;$i++){}
                            $user = new ClientUser($res->client_user);
                            $emailUserEmail                             = $user->getEmail();
                            $emailUserName                              = $user->getFullName();
                            $emailTimeFrom                              = date("H:i", strtotime($res->start));
                            $emailTimeTo                                = date("H:i", (strtotime($res->end)+60));
                            $emailDate                                  = Functions::dateAsStr($res->start);

                            //get venue details
                            $venue = Functions::getVenue($res->venue);
                            $emailAddressBuildingName                   = $venue->getBuildingName();
                            $emailAddressFirstLineOfAddress             = $venue->getFirstLine();
                            $emailAddressTown                           = $venue->getTown();
                            $emailAddressCounty                         = $venue->getCounty();
                            $emailAddressPostcode                       = $venue->getPostcode();

                            //staff details
                            $staff = Functions::getStaff($res->staff);
                            $emailStaffName     =   $staff->name;
                            $emailStaffPhone    =   $staff->phone;
                            $emailStaffEmail    =   $staff->email;
                            $emailStaffEmailLink = 'mailto:' . $emailStaffEmail;

                            //emailAddresss
                            $emailTo = array();

                            //links
                            if($auth == 'admin') {
                                //just email user
                                array_push($emailTo, array('email' => $emailUserEmail, 'content' => require_once(HTML . 'email.adminCancelAppointment.html')));
                            }
                            else {
                                //email both user and admin
                                array_push($emailTo, array('email' => $emailUserEmail, 'content' => require_once(HTML . 'email.adminCancelAppointment.html')));
                                array_push($emailTo, array('email' => $emailStaffEmail, 'content' => require_once(HTML . 'email.userCancelAppointment.html')));
                            }
                            global $sendMail;
                            //email staff members and user
                            foreach($emailTo as $i => $e) {
                                if(!$sendMail->carrierPigeon($e['email'], 'Appointment Cancellation', $e['content'], 'no-reply')) echo 0;
                            }

                            //update database
                            $query = "  DELETE FROM
                                          appointment
                                       WHERE
                                          id = '" . $id . "';
                                    ";
                            if($db->quickQuery($query)) echo 1;
                            else echo 0;

                        } else {
                            return 0;
                        }
                     } else {
                        echo 0;
                    }
                } else {
                    return 0;
                }
?>
