<?php

require_once('../config/config.php');


$id     = trim($_POST['id']);
$email  = trim($_POST['email']);
if(filter_var($email, FILTER_VALIDATE_EMAIL)) {

    if(!Functions::isEmail($email)) {

        if(Functions::escapeInjection($id)
            &&
            Functions::escapeInjection($email))
        {
            $key = Functions::createRandomKey('client_user');
            $query = "
                    UPDATE
                      client_user
                    set
                      validation_key = '" . $key . "', newEmail = '" . $email . "'
                    WHERE
                      id  = '" . $id . "'
                ";

            //get user details
            $user = new ClientUser($id);
            $emailUserName = $user->getFullName();
            $emailLinkA = THIS_DOMAIN. 'confirmation/?action=emailchange&key=' . $key;

            require_once('../html/email.verifynewemail.html');

            global $sendMail;

            if($db->quickQuery($query)) {
                do {
                    $res = $sendMail->carrierPigeon($email, 'Email Change', $html, 'no-reply');
                } while(!$res);
                echo 1;
            }
            //db fail
            else echo 0;
        } else {
            //string not accepted
            echo 2;
        }
    } else {
        //is already in use
        echo 4;
    }
} else {
    //not a proper email
    echo 3;
}





?>
