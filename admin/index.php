<?php

//make sure you spell view correctly!
require_once('../config/config.php');
require_once(CONTROLLERS . 'Class.Admin.Controller.php');
require_once(MODELS . 'Class.Admin.Model.php');
require_once(VIEWS . 'Class.Admin.View.php');
require_once(APPOINTMENT . 'Class.Appointment.php');

if(!LOGGED_IN) {
    $jsArray = array('jquery', 'main', 'jquery-ui.min', 'login');//set extra js which calls the login function
    $cssArray = array('main');
    $model = new AdminModel('Admin', $cssArray, $jsArray);
} else {
    $model = new AdminModel('Admin');
}

$view = new AdminView($model);
$controller = new AdminController($model, $view);


if(LOGGED_IN) {
    if(ADMIN) $controller->init();
}

$view->output();

?>