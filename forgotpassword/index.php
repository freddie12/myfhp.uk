<?php
require_once('../config/config.php');
require_once(CONTROLLERS . 'Class.ChangePassword.Controller.php');
require_once(MODELS . 'Class.ChangePassword.Model.php');
require_once(VIEWS . 'Class.ChangePassword.View.php');

$model = new ChangePasswordModel('Change Password');
$view = new ChangePasswordView($model);
$controller = new ChangePasswordController($model, $view);

$controller->init();
$view->output();
?>
